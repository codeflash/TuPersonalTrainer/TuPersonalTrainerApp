export const NUTRITIONALPLAN_FETCHING_DATA = 'NUTRITIONALPLAN/FETCHING_DATA'
export const NUTRITIONALPLAN_FETCHING_DATA_SUCCESS = 'NUTRITIONALPLAN/FETCHING_DATA_SUCCESS'
export const NUTRITIONALPLAN_FETCHING_DATA_FAILURE = 'NUTRITIONALPLAN/FETCHING_DATA_FAILURE'
import {api_nutritionalplan} from '../api'
import {NavigationActions} from 'react-navigation'





export const fetchingData = () => {
    return {type: NUTRITIONALPLAN_FETCHING_DATA}
};
export const fetchingDataSuccess = (data) => {
    return {type: NUTRITIONALPLAN_FETCHING_DATA_SUCCESS,  data: data}
};
export const fetchingDataFailure= () => {
    return {type: NUTRITIONALPLAN_FETCHING_DATA_FAILURE}
};
export const getNutritionalPlan = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_nutritionalplan(token).then((response) => {
            return response.data
        }).then((data) => {
            console.log(data)
            if (data.success) {
                dispatch(fetchingDataSuccess(data.data))
                
            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log(error),
            dispatch(fetchingDataFailure())
        })
    }
};





