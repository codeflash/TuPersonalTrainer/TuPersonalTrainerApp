
export const CHANGE_PASSWORD = 'PROFILE_CHANGE_PASSWORD'
export const CHANGE_EMAIL = 'PROFILE_CHANGE_EMAIL'
export const CHANGE_USERNAME = 'PROFILE_CHANGE_USERNAME'
export const PROFILE_FETCHING_DATA = 'PROFILE/FETCHING_DATA'
export const INITIAL_STATE = 'PROFILE/INITIAL_STATE'
export const PROFILE_FETCHING_DATA_SUCCESS = 'PROFILE/FETCHING_DATA_SUCCESS'
export const PROFILE_FETCHING_DATA_FAILURE = 'PROFILE/FETCHING_DATA_FAILURE'
import {api_me} from '../api'
import {NavigationActions} from 'react-navigation'

export const getInitialState = () => {
    return {type: INITIAL_STATE}
}

export const changeUsername = (username) => {
    return {type: CHANGE_USERNAME, username: username}
};

export const changePassword = (password) => {
    return {type: CHANGE_PASSWORD, password: password}
};

export const changeEmail = (email) => {
    return {type: CHANGE_EMAIL, email: email}
};

export const fetchingData = () => {
    return {type: PROFILE_FETCHING_DATA}
};
export const fetchingDataSuccess = (data) => {
    return {type: PROFILE_FETCHING_DATA_SUCCESS,  data: data}
};
export const fetchingDataFailure= () => {
    return {type: PROFILE_FETCHING_DATA_FAILURE}
};
export const me = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_me(token).then((response) => {
            console.log('respuesta' , response)
            return response.data
        }).then((data) => {
            console.log('data' , data)
            if (data._id) {
                //moveScreen('Dashboard' , navigation)
                dispatch(fetchingDataSuccess(data))
                
            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log('error', error)
            dispatch(fetchingDataFailure())
        })
    }
};





