export const WEIGHT_FETCHING_DATA = 'WEIGHT/FETCHING_DATA'
export const WEIGHT_FETCHING_DATA_SUCCESS = 'WEIGHT/FETCHING_DATA_SUCCESS'
export const WEIGHT_FETCHING_DATA_FAILURE = 'WEIGHT/FETCHING_DATA_FAILURE'
export const WEIGHT_CREATE_DATA_SUCCESS = 'WEIGHT/CREATE_DATA_SUCCESS'
import {api_add_weight, api_weight} from '../api'
import {NavigationActions} from 'react-navigation'



export const fetchingData = () => {
    return {type: WEIGHT_FETCHING_DATA}
};
export const fetchingDataSuccess = (data) => {
    return {type: WEIGHT_FETCHING_DATA_SUCCESS, data: data}
};

export const createDataSuccess = (item) => {
    return {type: WEIGHT_CREATE_DATA_SUCCESS, item: item}
};

export const fetchingDataFailure = () => {
    return {type: WEIGHT_FETCHING_DATA_FAILURE}
};

export const last_weight = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_last_weight(token).then((response) => {
            console.log('respuesta', response)
            return response.data
        }).then((data) => {
            console.log('data', data)
            if (data._id) {
                //moveScreen('Dashboard' , navigation)
                dispatch(fetchingDataSuccess(data.tip_messages))

            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log('error', error)
            dispatch(fetchingDataFailure())
        })
    }
};

function compareDate(str1) {
    // str1 format should b yyyy-mm-dd. Separator can be anything e.g. / or -. It
    // wont effect
    var dt1 = parseInt(str1.substring(8, 10));
    var mon1 = parseInt(str1.substring(5, 7));
    var yr1 = parseInt(str1.substring(0, 4));
    var date1 = new Date(yr1, mon1 - 1, dt1);
    return date1;
}

export const weight = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_weight(token).then((response) => {

            return response.data
        }).then((data) => {
            
            if (data.success) {
                datos = []
                data
                    .data
                    .forEach(function (registro) {
                        datos.push({y: registro.peso, x: compareDate(registro.fecha).getDate()})
                    });
                    console.log(datos);
                //moveScreen('Dashboard' , navigation)
                dispatch(fetchingDataSuccess(datos))

            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {

            dispatch(fetchingDataFailure())
        })
    }
};

export const add_weight = (token, weight, navigation) => {
    return (dispatch) => {
        dispatch(fetchingData())

        api_add_weight(token, weight).then((response) => {
            return response.data
        }).then((data) => {
           
            if (data.success) {
                dispatch(createDataSuccess(weight))
                navigation.goBack();
            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log('error', error)
            dispatch(fetchingDataFailure())
        })
    }
};
