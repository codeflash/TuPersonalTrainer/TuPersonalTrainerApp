export const CHANGE_PASSWORD = 'AUTH_CHANGE_PASSWORD'
export const CHANGE_EMAIL = 'AUTH_CHANGE_EMAIL'
export const CHANGE_USERNAME = 'AUTH_CHANGE_USERNAME'
export const CHANGE_GENRE = 'CHANGE_GENRE'
export const CHANGE_TALL = 'CHANGE_TALL'
export const CHANGE_GOAL = 'CHANGE_GOAL'
export const CHANGE_WEIGHT = 'CHANGE_WEIGHT'
export const CHANGE_YEARS = 'CHANGE_YEARS'
export const INITIAL_STATE = 'USER/INITIAL_STATE'
export const CHANGE_BODYFAT = 'CHANGE_BODYFAT'
export const USER_FETCHING_DATA = 'USER/FETCHING_DATA'
export const CHANGE_PASSWORDCONFIRM = 'CHANGE_PASSWORDCONFIRM'
export const USER_FETCHING_DATA_SUCCESS = 'USER/FETCHING_DATA_SUCCESS'
export const USER_FETCHING_DATA_FAILURE_SIGNIN = 'USER/FETCHING_DATA_FAILURE_SIGNIN'
export const USER_FETCHING_DATA_FAILURE_SIGNUP = 'USER/FETCHING_DATA_FAILURE_SIGNUP'
import {AsyncStorage} from 'react-native'
import {api_login, api_register} from '../api'
import {NavigationActions} from 'react-navigation'

export const getInitialState = () => {
    return {type: INITIAL_STATE}
}

export const changeUsername = (username) => {
    return {type: CHANGE_USERNAME, username: username}
};

export const changePassword = (password) => {
    return {type: CHANGE_PASSWORD, password: password}
};

export const changeEmail = (email) => {
    return {type: CHANGE_EMAIL, email: email}
};

export const changePasswordConfirm = (passwordConfirm) => {
    return {type: CHANGE_PASSWORDCONFIRM, passwordConfirm: passwordConfirm}
};

export const changeBodyFat = (bodyFat) => {
    return {type: CHANGE_BODYFAT, bodyFat: bodyFat}
};
export const changeGenre = (genre) => {
    return {type: CHANGE_GENRE, genre: genre}
};
export const changeWeight = (weight) => {
    return {type: CHANGE_WEIGHT, weight: weight}
};
export const changeTall = (tall) => {
    return {type: CHANGE_TALL, tall: tall}
};
export const changeGoal = (goal) => {
    return {type: CHANGE_GOAL, goal: goal}
};
export const changeYears = (years) => {
    return {type: CHANGE_YEARS, years: years}
};

export const fetchingData = () => {
    return {type: USER_FETCHING_DATA}
};
export const fetchingDataSuccess = (email, password, token) => {
    return {type: USER_FETCHING_DATA_SUCCESS, email: email, password: password, token: token}
};
export const fetchingDataFailureSignIn = () => {
    return {type: USER_FETCHING_DATA_FAILURE_SIGNIN}
};
export const fetchingDataFailureSignUp = () => {
    return {type: USER_FETCHING_DATA_FAILURE_SIGNUP}
};
export const login = (email, password, navigation) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_login(email, password).then((response) => {
            return response.data
        }).then((data) => {
            if (data.auth) {
                dispatch(fetchingDataSuccess(email, password, data.token, data.auth))   
            } else {
                dispatch(fetchingDataFailureSignIn())
            }
        }).catch((error) => {
            dispatch(fetchingDataFailureSignIn())
        })
    }
};


export const register = (username, email, password ,weight , tall , goal, years, bodyFat, genre) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_register(username, email, password ,weight , tall , goal, years, bodyFat, genre).then((response) => {
            return response.data
        }).then((data) => {
            //console.log(data)
            if (data.auth) {
                //moveScreen('Dashboard' , navigation)
                //storeToken(data.token)
                dispatch(fetchingDataSuccess(email, password, data.token, data.auth))
                
            } else {
                dispatch(fetchingDataFailureSignUp())
            }

        }).catch((error) => {
            dispatch(fetchingDataFailureSignUp())
        })
    }
}


