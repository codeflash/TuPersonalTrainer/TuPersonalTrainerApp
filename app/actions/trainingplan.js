export const TRAININGPLAN_FETCHING_DATA = 'TRAININGPLAN/FETCHING_DATA'
export const TRAININGPLAN_FETCHING_DATA_SUCCESS = 'TRAININGPLAN/FETCHING_DATA_SUCCESS'
export const TRAININGPLAN_FETCHING_DATA_FAILURE = 'TRAININGPLAN/FETCHING_DATA_FAILURE'
import {api_trainingplan} from '../api'
import {NavigationActions} from 'react-navigation'





export const fetchingData = () => {
    return {type: TRAININGPLAN_FETCHING_DATA}
};
export const fetchingDataSuccess = (data) => {
    return {type: TRAININGPLAN_FETCHING_DATA_SUCCESS,  data: data}
};
export const fetchingDataFailure= () => {
    return {type: TRAININGPLAN_FETCHING_DATA_FAILURE}
};
export const getTrainingPlan = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_trainingplan(token).then((response) => {
            return response.data
        }).then((data) => {
            console.log(data)
            if (data.success) {
                dispatch(fetchingDataSuccess(data.data))
                
            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log(error),
            dispatch(fetchingDataFailure())
        })
    }
};





