export const TIPS_FETCHING_DATA = 'TIPS/FETCHING_DATA'
export const TIPS_FETCHING_DATA_SUCCESS = 'TIPS/FETCHING_DATA_SUCCESS'
export const TIPS_FETCHING_DATA_FAILURE = 'TIPS/FETCHING_DATA_FAILURE'
import {api_tips} from '../api'
import {NavigationActions} from 'react-navigation'





export const fetchingData = () => {
    return {type: TIPS_FETCHING_DATA}
};
export const fetchingDataSuccess = (data) => {
    return {type: TIPS_FETCHING_DATA_SUCCESS,  data: data}
};
export const fetchingDataFailure= () => {
    return {type: TIPS_FETCHING_DATA_FAILURE}
};
export const tips = (token) => {
    return (dispatch) => {
        dispatch(fetchingData())
        api_tips(token).then((response) => {
            console.log('respuesta' , response)
            return response.data
        }).then((data) => {
            console.log('data' , data)
            if (data._id) {
                //moveScreen('Dashboard' , navigation)
                dispatch(fetchingDataSuccess(data.tip_messages))
                
            } else {
                dispatch(fetchingDataFailure())
            }

        }).catch((error) => {
            console.log('error', error)
            dispatch(fetchingDataFailure())
        })
    }
};





