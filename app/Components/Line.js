import React, {Component} from 'react';
import {View} from 'react-native';

export default class Line extends Component {
    render() {
        return (<View
            style={[
            {
                
                width: (this.props.vertical)
                    ? this.props.width
                    : null,
                height: (this.props.horizontal)
                    ? this.props.height
                    : (this.props.vertical) ? this.props.height : null,
                backgroundColor: this.props.color
            },
            this.props.style
        ]}/>)
    }
}

Line.defaultProps = {
    color: '#ccc',
    height: 2,
    width: 2
};