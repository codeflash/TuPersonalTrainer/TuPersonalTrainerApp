import React, {Component} from 'react';

import {NavigationActions} from 'react-navigation';
import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Image
} from 'react-native';

import CONSTANTS from '../Constants';
import {shade} from '../Utils';
import {connect} from 'react-redux'

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({routeName: route});
    this
      .props
      .navigation
      .dispatch(navigateAction);
  }

  render() {
    return (
      <ScrollView>
        <View>
          <View style={styles.containerImageAndName}>
            <Image
              style={styles.imageProfile}
              source={require('../../assets/images/profile.jpg')}/>
            <Text style={styles.nameProfile}>{this.props.username}</Text>
          </View>
          <View style={styles.line}/>
          <TouchableHighlight
            underlayColor={shade(CONSTANTS.contrastColor, -0.15)}
            style={styles.navItemStyle}
            onPress={this.navigateToScreen('Main')}>
            <Text style={styles.textItem}>
              Inicio
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor={shade(CONSTANTS.contrastColor, -0.15)}
            style={styles.navItemStyle}
            onPress={this.navigateToScreen('Profile')}>
            <Text style={styles.textItem}>
              Perfil
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor={shade(CONSTANTS.contrastColor, -0.15)}
            style={styles.navItemStyle}
            onPress={this.navigateToScreen('Workouts')}>
            <Text style={styles.textItem}>
              Plan de entrenamiento
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor={shade(CONSTANTS.contrastColor, -0.15)}
            style={styles.navItemStyle}
            onPress={this.navigateToScreen('MealPlan')}>
            <Text style={styles.textItem}>
              Plan Nutricional
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover', // or 'stretch'
  },
  navItemStyle: {
    padding: 20
  },
  textItem: {
    fontSize: CONSTANTS.fontSizeNormal
  },

  containerImageAndName: {
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },

  line: {
    height: 2,
    backgroundColor: '#ccc'
  },

  imageProfile: {
    height: 80,
    borderRadius: 80 / 2,
    width: 80,
    marginRight: 20
  },
  nameProfile: {
    fontSize: CONSTANTS.fontSizeNormal
  }
});

const mapStateToProps = (state) => {
  return {username: state.profile.username}
}

export default connect(mapStateToProps)(SideMenu);
