import React, {Component} from 'react';
import {View, StyleSheet, Platform, StatusBar} from 'react-native';
import CONSTANTS from '../Constants'
import {shade} from '../Utils'

export default class Container extends Component {
    render() {
        const styles = StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: this.props.backgroundColor,
                paddingTop: (Platform.OS == 'ios')
                    ? 20
                    : 0
            }
        })
        return (
            <View style={[styles.container, this.props.style]}>
                <View style={{flex:1}}>
                    {this.props.children}
                </View>
            </View>
        )
    }
}

Container.defaultProps = {
    backgroundColor: '#e4e4e4'
};
