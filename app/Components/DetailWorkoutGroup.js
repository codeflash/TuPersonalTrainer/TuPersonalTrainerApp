import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native'
import Line from './Line';
import {SubtitleAndText} from './Text';

export default class DetailWorkoutGroup extends Component {
    render() {
        const {time, exercises} = this.props;
        return (
            <View>
                <Line horizontal/>
                <View style={styles.containerBox}>
                    <View style={styles.box}>
                        <SubtitleAndText left={time} right='hora'/>
                    </View>
                    <Line vertical/>
                    <View style={styles.box}>
                        <SubtitleAndText left={exercises} right='Ejercicios'/>
                    </View>
                </View>
                <Line horizontal/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    containerBox: {
        padding: 20,
        flexDirection: 'row',
       
    },

    box: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 20,
        
    }
})