import React, {Component} from 'react';
import {View, StyleSheet, Platform, TouchableHighlight, Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import {shade} from '../Utils';
import CONSTANTS from '../Constants';
import Line from './Line';

export default class Navbar extends Component {

    render() {
        const leftNameIcon = (Platform.OS == 'ios') ? 'ios-' + this.props.left : 'md-' + this.props.left
        const rightNameIcon = (Platform.OS == 'ios') ? 'ios-' + this.props.right : 'md-' + this.props.right
        function selectElement(props) {
            if (props.right) {
                return <Icon name={rightNameIcon} size={props.sizeIcon} color={props.color}/>
            } else {
                return <Text
                    style={{
                    fontSize: props.size,
                    color: props.color
                }}>{props.rightText}</Text>
            }
        }
        
        return (
            <View>
                <View
                    style={[
                    styles.navbar, {
                        backgroundColor: this.props.backgroundColor,
                        elevation: this.props.elevation
                    }
                ]}>
                    <TouchableHighlight
                        style={styles.button}
                        underlayColor={shade(this.props.backgroundColor, -0.2)}
                        onPress={this.props.onPressLeft}>
                        <Icon
                            name={leftNameIcon}
                            size={this.props.sizeIcon}
                            color={this.props.color}/>
                    </TouchableHighlight>
                    <View style={styles.title}>
                        <Text
                            style={{
                            fontSize: this.props.size + 2,
                            color: this.props.color,
                            fontWeight: 'bold'
                        }}>{this.props.title}</Text>
                    </View>
                    <TouchableHighlight
                        style={styles.button}
                        underlayColor={shade(this.props.backgroundColor, 0.2)}
                        onPress={this.props.onPressRight}>
                        {selectElement(this.props)}
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

Navbar.defaultProps = {
    color: CONSTANTS.textTitleColor,
    backgroundColor: CONSTANTS.textColorButton,
    size: 18,
    sizeIcon: 25,
    elevation: 0
};

const styles = StyleSheet.create({
    navbar: {
        flexDirection: 'row',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 70,
        height: 55,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
