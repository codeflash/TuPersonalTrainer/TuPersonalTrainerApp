import React, {Component} from 'react';

import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Subtitle, Title} from './Text';
import CONSTANTS from '../Constants';

export default class GenreGroup extends Component {
    render() {

        return (
            <View>
                <Title alignCenter marginVertical>Soy..</Title>
                <View
                    style={{
                    flexDirection: 'row',
                    marginHorizontal: 16
                }}>
                    <TouchableOpacity onPress={this.props.onPressMan} activeOpacity={0.7} style={styles.box}>
                        <Icon name='md-man' size={100} color='#fff'/>
                        <Subtitle color='#fff'>Hombre</Subtitle>
                    </TouchableOpacity>
                    <View style={styles.separator}/>
                    <TouchableOpacity onPress={this.props.onPressWoman} activeOpacity={0.7} style={styles.box}>
                        <Icon name='md-woman' size={100} color='#fff'/>
                        <Subtitle color='#fff'>Mujer</Subtitle>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box: {
        padding: 20,
        flex: 1,
        backgroundColor: CONSTANTS.brandColor ,
        justifyContent: 'center',
        alignItems: 'center',
        
        borderRadius: 4
    },
    separator: {
        width: 10
    }
})
