import React, {Component} from 'react';
import {View, Text, TextInput} from 'react-native';

import CONSTANTS from '../Constants';
import {Title} from './Text';
import Line from './Line';
import {Button} from './Button';
import {Body} from './Body'
import {TextContext} from './TextHelper'
import {Input} from './Input'
import {connect} from 'react-redux'
import {changeTall} from '../actions/auth'
import {InputWithSufix} from './Input'

class TallItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isValid: true
        }

    }
    isValidNumber = (number) => {
        return parseInt(number) >= 123 && parseInt(number) <= 300 && /^[0-9]{1,45}$/.test(number)  && this.props.tall
    }
    handleChangeText = (text) => {
        this
            .props
            .changeTall(text)
        this.setState({isValid: this.isValidNumber(text)})
    }
  
    render() {
        return (
            <Body>
                <Title alignCenter marginVertical>¿Cuanto mides?</Title>
                <InputWithSufix
                    autoFocus={true}
                    sufix='cm'
                    keyboardType='numeric'
                    maxLength={3}
                    onChangeText={this.handleChangeText}
                    error={!this.state.isValid}
                    message='Ingresa una altura valida'/>
                <Button
                        disabled={!this.state.isValid || !this.props.tall}
                        onPress={this.props.onPressNext}
                        text='Siguiente'/>   
            </Body>
        )
    }
}

const mapStateToProps = state => {
    return {tall: state.auth.tall}
}

const mapDispatchToProps = dispatch => {
    return {
        changeTall: (tall) => {
            dispatch(changeTall(tall));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TallItem);