import React, {Component} from 'react';
import {View, Image, Dimensions} from 'react-native';
import Line from '../Line';
import styles from './styles'



const RuleSection = (props) => {
    const {width} = Dimensions.get('window');
    
    //styles.$widthSectionRulePart = width * 50 / 400
    const sectionPartStyle = [styles.sectionPart]
    sectionPartStyle.push({width: width * 50 / 400})
    const imageStyle = [styles.image]
    imageStyle.push({width: width * 260 / 400})
    const separator = width * 2 / 400
    const heightLine = 25

    return (

        <View>
            <View style={styles.container}>
                <View style={sectionPartStyle}/>
                <Line vertical width={separator} height={heightLine}/>
                <View style={sectionPartStyle}/>
                <Line vertical width={separator} height={heightLine}/>
                <View style={sectionPartStyle}/>
                <Line vertical width={separator} height={heightLine}/>
                <View style={sectionPartStyle}/>
                <Line vertical width={separator} height={heightLine}/>
                <View style={sectionPartStyle}/>
                <Line vertical width={separator} height={heightLine * 2}/>
            </View>
            <Image resizeMode='cover' source={props.source} style={imageStyle}/>
        </View>
    )

}

export default RuleSection
