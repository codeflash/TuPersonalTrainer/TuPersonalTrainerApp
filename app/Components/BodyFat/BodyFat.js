import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    ScrollView,
    Platform,
    Dimensions
} from 'react-native';



import {Button} from '../Button';
import Triangle from '../Triangle';
import {Title} from '../Text';
import {Body} from '../Body'
import RuleSection from './RuleSection'
import {connect} from 'react-redux'
import {changeBodyFat} from '../../actions/auth'

import styles from './styles'
class BodyFat extends Component {

    constructor(props) {
        super(props);

        const {width} = Dimensions.get('window');
        const paddingLeft = width * 200 / 400
        const totalRuleSection = width * 260 / 400
        const totalRuleSectionPart = width * 50 / 400
        const separator = width * 2 / 400
        this.state = {
            fatPercent: 0,
            paddingLeft: paddingLeft,
            totalRuleSection: totalRuleSection,
            totalRuleSectionPart: totalRuleSectionPart,
            separator: separator
        }
    }

    componentDidMount() {
        if (this.props.genre === 'mujer') {
            setTimeout(() => this.scrollView.scrollTo({
                x: this.state.totalRuleSection * 2 + this.state.totalRuleSectionPart * 3 + this.state.separator * 3,
                y: 0
            }), 0);
        } else {
            setTimeout(() => this.scrollView.scrollTo({
                x: this.state.totalRuleSection * 3 + this.state.totalRuleSectionPart + this.state.separator,
                y: 0
            }), 0);
        }
    }

    handleScroll = (event) => {
        if (this.props.genre === 'mujer') {
            this.props.changeBodyFat(Math.round(event.nativeEvent.contentOffset.x / (this.state.totalRuleSectionPart + this.state.separator) + 7))
            
        } else {
            this.props.changeBodyFat(Math.round(event.nativeEvent.contentOffset.x / (this.state.totalRuleSectionPart + this.state.separator) + 4))
        }

    }

    womanOrMan() {
        if (this.props.genre === 'mujer') {
            return (
                <View
                    style={{
                    flex: 1,
                    flexDirection: 'row',
                    height: 250
                }}>

                    <View
                        style={{
                        width: this.state.paddingLeft
                    }}/>
                    <RuleSection source={require('../../../assets/images/woman/1.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/2.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/3.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/4.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/5.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/6.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/7.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/8.jpg')}/>
                    <RuleSection source={require('../../../assets/images/woman/9.jpg')}/>

                    <View
                        style={{
                        width: this.state.paddingLeft
                    }}/>
                </View>

            )
        } else {
            return (
                <View
                    style={{
                    flex: 1,
                    flexDirection: 'row',
                    height: 250
                }}>

                    <View
                        style={{
                        width: this.state.paddingLeft
                    }}/>
                    <RuleSection source={require('../../../assets/images/man/1.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/2.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/3.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/4.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/5.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/6.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/7.jpg')}/>
                    <RuleSection source={require('../../../assets/images/man/8.jpg')}/>

                    <View
                        style={{
                        width: this.state.paddingLeft
                    }}/>
                </View>
            )
        }
    }

    render() {
        return (
            <View>
                <Title alignCenter marginVertical>Estima tu porcentaje de grasa</Title>
                <View style={styles.containerBodyFat}>
                    <View
                        style={styles.boxNumber}>
                        <Text
                            style={styles.textBoxNumber}>{this.props.bodyFat + '%'}</Text>
                    </View>
                    <Triangle/>
                </View>
                <View style={{
                    height: 250
                }}>

                    <ScrollView
                        ref={(ref) => this.scrollView = ref}
                        onScroll={(event) => this.handleScroll(event)}
                        horizontal
                        showsHorizontalScrollIndicator={false}>
                        {this.womanOrMan()}
                    </ScrollView>
                </View>
                <Body>
                    <Button onPress={() => this.props.onPressNext('Register')} text='Siguiente'/>
                </Body>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        genre: state.auth.genre,
        bodyFat: state.auth.bodyFat
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeBodyFat: (bodyFat) => {
            dispatch(changeBodyFat(bodyFat));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(BodyFat)

