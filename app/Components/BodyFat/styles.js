import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
   
    $heightSectionRulePart: 50,
  
    $heightSectionRule: 200,
    sectionPart: {
        
        height: '$heightSectionRulePart'
    },
    image: {
     
        height: '$heightSectionRule'
    },
    container: {
        flexDirection: 'row',
        height: '$heightSectionRulePart',
        alignItems: 'flex-end'
    },
    containerBodyFat: {
        alignItems: 'center'
    },
    boxNumber: {
        alignItems: 'center',
        width: 70,
        height: 70,
        borderRadius: 5,
        justifyContent: 'center',
        backgroundColor: '$brandColor'
    },
    textBoxNumber: {
        color: 'white',
        fontSize: 30
    }

})