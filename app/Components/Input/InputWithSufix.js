import React from 'react'
import {View, TextInput, Text} from 'react-native'
import styles from './styles'
import {TextContext} from '../TextHelper'
import Line from '../Line'

const InputWithSufix = (props) => {
    const colorError = 'red'
    const containerStyles = [styles.containerInputWithText]
    const inputStyles = [styles.inputWithText]
    const sufixStyles = [styles.sufix]
    if (props.error) {
        containerStyles.push({backgroundColor: 'rgba(250,0,0,0.1)'})
        //sufixStyles.push({color: colorError})
    }

    return (
        <View style={styles.container}>
            <View style={containerStyles}>
                <TextInput
                    keyboardType={props.keyboardType}
                    style={inputStyles}
                    placeholder={props.placeholder}
                    maxLength={props.maxLength}
                    value={props.value}
                    autoFocus={props.autoFocus}
                    underlineColorAndroid='transparent'
                    onChangeText={props.onChangeText}/>
                <Text style={sufixStyles}>{props.sufix}</Text>
            </View>
            <Line horizontal color={props.error ? colorError : '#ccc'} width={1}/> 
            {props.error && <TextContext type='warning'>{props.message}</TextContext>}
        </View>

    )
}

export default InputWithSufix