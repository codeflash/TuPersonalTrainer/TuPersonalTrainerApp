import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    $underlayColor: '$brandColor',
    container: {
        marginBottom: 10
    },
    containerInput: {
        backgroundColor: '$white'
    },
    inputText: {
        fontSize: 20,
        paddingVertical: 20
    },
    containerInputWithText: {
        backgroundColor: '$white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    sufix: {
        paddingVertical: 20,
        fontSize: 30,
    },
    inputWithText:{
        color: '#000000',
        textAlign: 'right',
        paddingVertical: 20,
        width: 100,
        fontSize: 30,
        marginRight: 5
    }
})