import React from 'react'
import {View, TextInput} from 'react-native'
import styles from './styles'
import {TextContext} from '../TextHelper'

const Input = (props) => {
    const containerStyles = [styles.containerInput]
    if (props.error) {
        containerStyles.push({backgroundColor: 'rgba(250,0,0,0.1)'})
    }

    const underlineColorAndroid = (props.error) ? 'red' : styles.$underlayColor

    return (
        <View style={styles.container}>
            <View style={containerStyles}>
                <TextInput
                    keyboardType={props.keyboardType}
                    style={styles.inputText}
                    placeholder={props.placeholder}
                    secureTextEntry={props.secureTextEntry}
                    value={props.value}
                    autoFocus={props.autoFocus}
                    underlineColorAndroid={underlineColorAndroid}
                    onChangeText={props.onChangeText}/>
            </View>
            {props.error && <TextContext type='warning'>{props.message}</TextContext>}
        </View>

    )
}

export default Input