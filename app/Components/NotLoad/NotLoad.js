import React , { Component } from 'react'
import {Text} from 'react-native'
import {Body} from '../Body'
import {Button} from '../Button'
import styles from './styles'


const NotLoad = (props) => {


    return (
        <Body center>
            <Text style={styles.text}>Ocurrio un problema</Text>
            <Button type='normal'  text='cargar de nuevo' onPress={props.onPress}/>
        </Body>
    )
}

export default NotLoad