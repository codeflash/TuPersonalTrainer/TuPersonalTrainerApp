import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    button: {
        width: '50%',
        backgroundColor: '$white',
        borderColor: '#ccc',
        borderWidth: 1 
    },
    text: {
        fontSize: 30,
        marginBottom: 10
    }
})