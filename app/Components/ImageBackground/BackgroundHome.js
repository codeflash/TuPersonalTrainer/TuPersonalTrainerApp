import React from 'react'
import ImageBackground from './ImageBackground'

const image = require('./images/login-image.jpg')

const BackgroundHome = (props) => {
    return (
        <ImageBackground source={image}>
            {props.children}
        </ImageBackground>
    )
}

export default BackgroundHome