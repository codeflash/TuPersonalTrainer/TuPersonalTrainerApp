import React from 'react'
import {Image} from 'react-native'
import styles from './styles'

const ImageBackground = (props) => {

    const containerStyle = [styles.container]
    if(props.center){
        containerStyle.push(styles.center)
    }
    return (
        <Image
            style={containerStyle}
            source={props.source}
            blurRadius={2}>
            {props.children}
        </Image>
    )
}

export default ImageBackground
