import ImageBackground from './ImageBackground'
import BackgroundHome from './BackgroundHome'
import styles from './styles'

export {ImageBackground, BackgroundHome, styles}