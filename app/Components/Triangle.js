import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';

import CONSTANTS from '../Constants'

export default class Triangle extends Component {
    render() {
        return (<View style={[styles.triangle, this.props.style]}/>)
    }
}

const styles = StyleSheet.create({
    triangle: {
        marginTop: -1,
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 35,
        borderRightWidth: 35,
        borderTopWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderTopColor: CONSTANTS.brandColor
    }
})