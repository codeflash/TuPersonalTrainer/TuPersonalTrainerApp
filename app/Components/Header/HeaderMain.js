import React from 'react'
import {View, Text, ActivityIndicator} from 'react-native'
import styles from './styles'
import TipsBox from '../TipsBox'
import {Body} from '../Body'
import {Button} from '../Button'

const HeaderMain = (props) => {
    return (
        <View style={styles.containerHeaderMain}>
            <Body>
                <View style={styles.separator}/>
                <Text style={styles.titleHeaderMain}>{props.title}</Text>
                <View style={styles.separator}/>
                
                {props.tips.length > 0  &&
                    <View>
                        <View style={styles.separator}/>
                        <TipsBox tips={props.tips}/>
                        <View style={styles.separator}/>
                    </View>
                }
                <View style={styles.separator}/>
                <Button
                    backgroundColor='white'
                    color='#000'
                    text='Comenzar'
                    onPress={props.onPress}/>
                <View style={styles.separator}/>
            </Body>
        </View>
    )
}

export default HeaderMain