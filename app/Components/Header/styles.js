import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    containerHeaderMain: {
        flex: 1,
        backgroundColor: '$brandColor'   
    },
    separator: {
        height: 10,
        width: '100%'
    },
    titleHeaderMain:{
        textAlign: 'center',
        color: '$white',
        fontSize: 30,
        fontWeight: 'bold'
    }
    
})