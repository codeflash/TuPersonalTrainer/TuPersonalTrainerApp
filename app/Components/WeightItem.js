import React, {Component} from 'react';
import {View, Text, TextInput, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import {Title} from './Text';
import Line from './Line';
import {Button} from './Button';
import {Body} from './Body'
import {TextContext} from './TextHelper'
import {connect} from 'react-redux'
import {changeWeight} from '../actions/auth'
import {InputWithSufix} from './Input'

class WeightItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isValid: true
        }

    }

    isValidNumber = (number) => {
        return parseFloat(number) >= 20 && parseFloat(number) <= 299 && /^(0|[1-9]\d*)(\.\d+)?$/.test(number) && this.props.weight
    }
    handleChangeText = (text) => {
        this
            .props
            .changeWeight(text)
        this.setState({
            isValid: this.isValidNumber(text)
        })

    }

    render() {
        return (
            <Body>
                <Title alignCenter marginVertical>Peso</Title>
                <InputWithSufix
                    sufix='Kg'
                    autoFocus={true}
                    keyboardType='numeric'
                    maxLength={5}
                    onChangeText={this.handleChangeText}
                    error={!this.state.isValid}
                    message='Ingresa una peso valido'/>
                <Button
                    isFetching={this.props.isFetching}
                    disabled={!this.state.isValid || !this.props.weight}
                    onPress={this.props.onPressNext}
                    text={this.props.textButton || 'Siguiente'}/>
            </Body>

        )
    }
}

const mapStateToProps = state => {
    return {weight: state.auth.weight}
}

const mapDispatchToProps = dispatch => {
    return {
        changeWeight: (weight) => {
            dispatch(changeWeight(weight));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeightItem);