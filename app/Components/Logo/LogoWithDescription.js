import React from 'react'
import {View, Text} from 'react-native'
import Logo from './Logo'
import styles from './styles'

const LogoWithDescription = (props) => {
    return (
        <View style={styles.container}>
            <Logo/>
            <Text style={styles.title}>MetasFit</Text>
            <Text style={styles.message}>Algun mensaje para poner como una frase u otra</Text>
        </View>
    )
}

export default LogoWithDescription