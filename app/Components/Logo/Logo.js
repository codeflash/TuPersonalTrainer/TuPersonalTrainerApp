import React from 'react'
import {View, Image, Text} from 'react-native'
import styles from './styles'

const image = require('./image/logo.png') 

const Logo = (props) => {
    const imageStyles = [styles.image]
    if(props.height && props.width){
        imageStyles.push({width: props.width , height: props.height})
    }
    return(
        <View style={styles.containerLogo}>
            <Image style={imageStyles} source={image} />
        </View>
    )
}

export default Logo