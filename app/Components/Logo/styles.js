import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    containerLogo: {
        width: 200,
        height: 250
    },
    title: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '$white',
        textAlign: 'center'
    },
    message: {
        fontSize: 18,
        color: '$white',
        textAlign: 'center'
    }
})