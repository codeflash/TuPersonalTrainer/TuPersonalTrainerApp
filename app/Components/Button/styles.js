import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    $backgroundColorBase: '$brandColor',
    $backgroundColorDisabled: '#d9d9d9',
    $backgroundColorModifierDisabled: 0.4,
    $backgroundColorModifier: 0.2,
    container: {
        marginVertical: 5,
        alignItems: 'center',
        width: '100%',
        backgroundColor: '$backgroundColorBase',
        borderRadius: 20
    },

    wrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        width: 19,
        marginRight: 11
    },
    text: {
        fontWeight: 'bold',
        color: '$white',
        fontSize: 16,
        paddingVertical: 15
    },
    warning: {
        backgroundColor: 'rgb(255,0,0)',
    },
    normal: {
        width: '50%',
        borderWidth: 1,
        borderColor: "gray",
        backgroundColor: '$white',
    },
    indicator: {
        paddingVertical: 16,
        paddingHorizontal: 20
    }
})