import React from 'react'
import {TouchableHighlight, Text, ActivityIndicator} from 'react-native'
import PropTypes from 'prop-types'
import {Ionicons} from 'react-native-vector-icons'
import color from 'color'
import styles from './styles'

const Button = (props) => {
    const backgroundColor = props.backgroundColor || styles.$backgroundColorBase
    let underlayColor = color(backgroundColor).darken(styles.$backgroundColorModifier)
    const backgroundDisabled = styles.$backgroundColorDisabled
    const colorDisabled = color(backgroundDisabled).darken(styles.$backgroundColorModifierDisabled)

    const containerStyles = [styles.container]
    const textStyles = [styles.text]
    if (props.backgroundColor) {
        containerStyles.push({backgroundColor: props.backgroundColor})
    }
    if (props.disabled) {
        containerStyles.push({backgroundColor: backgroundDisabled})
        underlayColor = backgroundDisabled
        textStyles.push({color: colorDisabled})
    }
    if (props.color) {
        textStyles.push({color: props.color})
    }
    if (props.borderRadius) {
        containerStyles.push({borderRadius: props.borderRadius})
    }

    if (props.type === 'warning') {
        containerStyles.push(styles.warning)
        underlayColor = 'rgba(255,0,0,0.7)'
    }

    if (props.type === 'normal') {
        containerStyles.push(styles.normal)
        textStyles.push({color: '#000'})
        underlayColor = '#ccc'
    }

    if (props.style) {
        containerStyles.push(props.style)
    };

    return (
        <TouchableHighlight
            style={containerStyles}
            activeOpacity={1}
            onPress={(props.disabled)
            ? () => null
            : props.onPress}
            underlayColor={underlayColor}>

            {(props.isFetching)
                ? <ActivityIndicator style={styles.indicator} color="#fff"/>
                : <Text style={textStyles}>{(props.text)
                        ? props.text.toUpperCase()
                        : props.children}</Text>}
        </TouchableHighlight>

    )
}

Button.propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
    borderRadius: PropTypes.number
};

export default Button