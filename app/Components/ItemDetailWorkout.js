import React, {Component} from 'react';

import ItemList from './ItemList';

export default class ItemDetailWorkout extends Component {
    render() {
        const {exercise} = this.props;
        return (<ItemList iconLeft='md-walk' left={exercise} onPress={this.props.onPress}/>)
    }
}
