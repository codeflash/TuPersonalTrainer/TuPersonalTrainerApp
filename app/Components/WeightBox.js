import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Card from './Card';
import CONSTANTS from '../Constants';
import {Title, Subtitle} from './Text';
import Icon from 'react-native-vector-icons/Ionicons';
import {Button} from './Button';
import {Body} from './Body';

export default class WeightBox extends Component {
    render() {
        const {currentWeight} = this.props;
        return (
            <TouchableOpacity onPress={this.props.onPress} activeOpacity={0.8}>
                <Card
                    style={{
                    marginHorizontal: 10,
                    marginVertical: 10
                }}>

                    <View style={styles.containerBox}>
                        <View style={styles.box}>
                            <Title>{currentWeight} Kg</Title>
                            <Subtitle >
                                Peso Actual</Subtitle>

                        </View>
                    </View>
                </Card>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    containerBox: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    box: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})