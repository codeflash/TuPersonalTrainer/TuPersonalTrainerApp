import React , { Component } from 'react';
import {
    View,
    TouchableHighlight,
    StyleSheet,
    Text,
    Image
} from 'react-native';

export default class ImageButton extends Component{

    render() {
    
        return (
            <TouchableHighlight
                style={[
                    {   width: this.props.width ,
                        height: this.props.height ,
                        borderRadius: (this.props.circle) ? this.props.width / 2 : 0
                    },
                    this.props.style
                ]}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <Image 
                    style={
                        {   width: this.props.width ,
                            height: this.props.height ,
                            borderRadius: (this.props.circle) ? this.props.width / 2 : 0
                        }
                    }
                    source={this.props.src} 
                />
            </TouchableHighlight>

        )
    }


}