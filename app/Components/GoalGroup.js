import React, {Component} from 'react';

import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Subtitle, Title} from './Text';
import CONSTANTS from '../Constants'

export default class GoalGroup extends Component {
    render() {

        return (
            <View>
                <Title alignCenter marginVertical>¿Cual es tu objetivo?</Title>
                <View style={{
                    marginHorizontal: 16
                }}>
                    <TouchableOpacity
                        onPress={this.props.onPressLoseWeight}
                        activeOpacity={0.6}
                        style={styles.box}>
                        <Subtitle color='#fff'>Perder Peso</Subtitle>
                    </TouchableOpacity>
                    <View style={styles.separator}/>
                    <TouchableOpacity
                        onPress={this.props.onPressStayInshape}
                        activeOpacity={0.7}
                        style={styles.box}>
                        <Subtitle color='#fff'>Mantenerse en forma</Subtitle>
                    </TouchableOpacity>
                    <View style={styles.separator}/>
                    <TouchableOpacity
                        onPress={this.props.onPressGainMuscleMass}
                        activeOpacity={0.7}
                        style={styles.box}>
                        <Subtitle color='#fff'>Ganar masa muscular</Subtitle>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box: {
        padding: 20,
        
        backgroundColor: CONSTANTS.brandColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    separator: {
        height: 10
    }
})
