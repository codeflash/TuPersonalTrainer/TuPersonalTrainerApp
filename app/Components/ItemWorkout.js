import React, {Component} from 'react';

import ItemList from './ItemList';

export default class ItemWorkout extends Component {
    render() {
        const {day} = this.props;
        return (<ItemList iconLeft='md-calendar' left={day} onPress={this.props.onPress}/>)
    }
}
