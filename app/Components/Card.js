import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import {Paragraph, Title} from '../Components/Text';
import Line from '../Components/Line';

export default class Card extends Component {
    constructor(props) {
        super(props);
    }
    withTitle() {
        if (this.props.title) {
            return (
                <View>
                    <Title marginVertical alignCenter>
                        {this.props.title}
                    </Title>
                </View>
            )
        } else {
            return null
        }
    }
    render() {
        return (
           
            <View
                style={[
                this.props.style, {
                    flex:1,
                    borderWidth: 1,
                    backgroundColor: this.props.backgroundColor,
                    borderColor: this.props.borderColor,
                    borderRadius: this.props.borderRadius,
                    elevation: this.props.elevation,
                    shadowOffset: {
                        width: 0,
                        height: this.props.elevation
                    },
                    shadowRadius: this.props.elevation,
                    shadowOpacity: 0.24
                }
            ]}>
                {this.withTitle()}
                <View
                    style={{
                    flex: 1,
                    paddingVertical: this.props.paddingVertical,
                    paddingHorizontal: this.props.paddingHorizontal
                }}>
                    {this.props.children}
                </View>
            </View>
    
        )
    }

}

Card.defaultProps = {
    backgroundColor: CONSTANTS.textColorButton,
    borderRadius: 6,
    borderColor: 'white',
    elevation: 3,
    paddingHorizontal: 20,
    paddingVertical: 26

};