import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import CONSTANTS from '../Constants';
import {shade} from '../Utils';

export class Title extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View >
                <Text
                    style={[
                    styles.title, {
                        marginVertical: (this.props.marginVertical)
                            ? 20
                            : 0,
                        marginHorizontal: (this.props.marginHorizontal)
                            ? 20
                            : 0,
                        textAlign: (this.props.alignCenter)
                            ? 'center'
                            : 'left',
                        color: this.props.color
                    },
                    this.props.style
                ]}>{(this.props.text)
                        ? this.props.text
                        : this.props.children}</Text>
            </View>
        )
    }
}

Title.defaultProps = {
    color: CONSTANTS.titleTextColor
}

export class Paragraph extends Component {
    constructor(props) {
        super(props)
    }

    listItemOrText() {
        if (this.props.listItem) {
            return (
                <View style={styles.item}>
                    <Text
                        style={[
                        styles.icon, {
                            fontWeight: (this.props.strong)
                                ? 'bold'
                                : 'normal'
                        },
                        this.props.style
                    ]}>
                        {this.props.listItemIcon || '+'}
                    </Text>
                    <Text
                        style={[
                        styles.textItem, {
                            fontWeight: (this.props.strong)
                                ? 'bold'
                                : 'normal'
                        },
                        this.props.style
                    ]}>
                        {this.props.children}
                    </Text>
                </View>
            )
        } else {
            return (
                <Text
                    style={[
                    styles.text, {
                        textAlign: this.props.textAlign || 'left',
                        fontWeight: (this.props.strong)
                            ? 'bold'
                            : 'normal'
                    },
                    this.props.style
                ]}>
                    {this.props.children}
                </Text>
            )
        }
    }

    render() {
        return (
            <View>
                {this.listItemOrText()}
            </View>
        )
    }
}

export class Subtitle extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View>
                <Text
                    style={[
                    styles.subtitle, {
                        marginVertical: (this.props.marginVertical)
                            ? 10
                            : 0,
                        marginHorizontal: (this.props.marginHorizontal)
                            ? 10
                            : 0,
                        textAlign: (this.props.alignCenter)
                            ? 'center'
                            : 'left',
                        color: this.props.color
                    },
                    this.props.style
                ]}>{this.props.children}</Text>
            </View>
        )
    }
}

Subtitle.defaultProps = {
    color: shade(CONSTANTS.titleTextColor, 0.3)
}

export class SubtitleAndText extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View
                style={[
                styles.item, {
                    marginVertical: (this.props.marginVertical)
                        ? 5
                        : 0,
                    marginHorizontal: (this.props.marginHorizontal)
                        ? 5
                        : 0
                }
            ]}>
                <Text style={[styles.subtitle, this.props.style]}>{this.props.left}</Text>
                <Text style={[styles.subtitle, this.props.style]}>{(this.props.dots)
                        ? ': '
                        : ' '}</Text>
                <Text
                    style={[
                    styles.text, {
                        fontSize: CONSTANTS.FontSizeSubtitle
                    },
                    this.props.style
                ]}>{this.props.right}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    title: {
        fontSize: CONSTANTS.fontSizeTitle,
        fontWeight: 'bold'
    },

    subtitle: {
        fontSize: CONSTANTS.FontSizeSubtitle,
        fontWeight: 'bold'
    },

    text: {
        lineHeight: 27,
        fontSize: CONSTANTS.fontSizeNormal,
        color: CONSTANTS.titleTextColor,
        color: shade(CONSTANTS.titleTextColor, 0.5)
    },

    item: {
        flex: 1,
        flexDirection: 'row'
    },

    icon: {
        lineHeight: 27,
        fontSize: CONSTANTS.fontSizeNormal,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        width: 25
    },
    textItem: {
        flex: 1,
        textAlign: 'left',
        lineHeight: 27,
        fontSize: CONSTANTS.fontSizeNormal,
        color: CONSTANTS.titleTextColor,
        color: shade(CONSTANTS.titleTextColor, 0.5)
    }
})