import React, {Component} from 'react';
import {View, FlatList, Dimensions} from 'react-native';
import Card from './Card';
import {Paragraph, Title} from './Text';

const {width} = Dimensions.get('window');

export default class TipsBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tips: props.tips
        }
    }
    keyExtractor = (item) => item;
    render() {
        return (
            <View
                style={{
                width: width - 32,
            }}>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    pagingEnabled
                    keyExtractor={this.keyExtractor}
                    data={this.props.tips}
                    renderItem={({item , key}) => <Tip key={key} tip={item.mensaje}/>}/>
            </View>
        )
    }
}

class Tip extends Component {

    render() {
        return (
            <Title
                color='white'
                style={{
                marginBottom: 5,
                width: width - 32
            }}
                alignCenter>"{this.props.tip}"</Title>
        )
    }
}
