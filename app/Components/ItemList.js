import React, {Component} from 'react';
import {View, TouchableHighlight, StyleSheet, Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import CONSTANTS from '../Constants';
import {shade} from '../Utils'
import {Paragraph, Subtitle, Title} from '../Components/Text';

export default class ItemList extends Component {
    constructor(props) {
        super(props)
    }
    textOrIcon() {

        if (this.props.right) {
            return <Paragraph style={styles.text}>{this.props.right}</Paragraph>
        } else {
            return <Icon name={this.props.iconRight} size={25} color='#000'/>

        }
    }

    textAndIcon(){
        if (this.props.iconLeft) {
            return (
                <View style={{flexDirection: 'row'}}>
                    <Icon name={this.props.iconLeft} size={25} color='#000'/>
                    <Subtitle style={[styles.text,{marginLeft: 10}]}>{this.props.left}</Subtitle>
                </View>
            )
        } else {
            return <Subtitle style={styles.text}>{this.props.left}</Subtitle>
        }

    }

    render() {
        return (
            <View>
                <TouchableHighlight
                    underlayColor={shade(CONSTANTS.contrastColor, -0.15)}
                    onPress={this.props.onPress}>
                    <View style={styles.containerItem}>
                        {this.textAndIcon()}
                        {this.textOrIcon()}
                    </View>
                </TouchableHighlight>

            </View>
        )
    }
}
ItemList.defaultProps = {
    iconRight: 'ios-arrow-forward'
}

const styles = StyleSheet.create({

    containerItem: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    text: {
        fontSize: CONSTANTS.fontSizeNormal
    }
})