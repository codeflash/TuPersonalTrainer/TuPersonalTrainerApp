import React, {Component} from 'react';
import {Image, Dimensions, View} from 'react-native';

import CONSTANTS from '../Constants';

export default class Tile extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {src, text, height, children} = this.props;
        const {width} = Dimensions.get('window');
        return (
            <View
                style={{
                flex: 1,
                width: width,
                height: height
            }}>
                <Image
                    
                    style={{
                    
                    flex: 1,
                    width: width,
                    resizeMode: 'cover',
                    height: height,
                    justifyContent: 'center'
                    
                }}
                    source={src}>
                    {children}
                </Image>
            </View>
        )
    }

}