import React, {Component} from 'react';
import {View, Text, TextInput, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import {Title} from './Text';
import Line from './Line';
import {Button} from './Button';
import {Body} from './Body'
import {TextContext} from './TextHelper'
import {connect} from 'react-redux'
import {changeYears} from '../actions/auth'
import {InputWithSufix} from './Input'

class YearsItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isValid: true
        }

    }

    isValidNumber = (number) => {
        return parseInt(number) >= 13 && parseInt(number) <= 99 && /^[0-9]{1,45}$/.test(number) && this.props.years
    }
    handleChangeText = (text) => {
        this.setState({
            isValid: this.isValidNumber(text)
        })
        this
            .props
            .changeYears(text)
       

    }

    render() {
        return (
            <Body>
                <Title alignCenter marginVertical>¿Cuanto años tienes?</Title>
                <InputWithSufix
                    sufix='años'
                    autoFocus={true}
                    keyboardType='numeric'
                    maxLength={2}
                    onChangeText={this.handleChangeText}
                    error={!this.state.isValid}
                    message='Ingresa una edad valida'/>
                <Button
                    disabled={!this.state.isValid || !this.props.years}
                    onPress={this.props.onPressNext}
                    text='Siguiente'/>
            </Body>
        )
    }
}

const mapStateToProps = state => {
    return {years: state.auth.years}
}

const mapDispatchToProps = dispatch => {
    return {
        changeYears: (years) => {
            dispatch(changeYears(years));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(YearsItem);