import React, {Component} from 'react';
import {View, FlatList, Text} from 'react-native';
import Card from './Card';
import ItemList from './ItemList';

export default class WorkoutBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentWorkout: props.currentWorkout
        }

    }

    keyExtractor = (item) => item.id;

    render() {
        const {navigation} = this.props;
        return (
            <Card
                title='Entrenamiento de Hoy'
                style={{
                marginHorizontal: 10,
                marginVertical: 10
            }}>
                <FlatList
                    data={this.state.currentWorkout}
                    keyExtractor={this.keyExtractor}
                    renderItem={({item}) => <ItemList left={item.name} onPress={this.props.onPress}/>}/>

            </Card>
        )
    }
}
