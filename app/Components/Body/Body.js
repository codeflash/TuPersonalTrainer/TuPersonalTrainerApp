import React from 'react'
import {View} from 'react-native'

import styles from './styles'

const Body = (props) => {

    const containerStyle = [styles.container]
    if(props.center){
        containerStyle.push(styles.center)
    }
    if(!props.noflex){
        containerStyle.push(styles.containerWithFlex)
    }

    return (
        <View style={containerStyle}>
            {props.children}
        </View>
    )
}
export default Body