import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    container: {
        marginVertical: 20,
        marginHorizontal: 16
    },
    containerWithFlex: {
        flex: 1
    },
    center:{
        justifyContent: 'center',
        alignItems: 'center'
    }
})