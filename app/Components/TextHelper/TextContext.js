import React from 'react'
import {Text} from 'react-native'
import styles from './styles'

const TextContext = (props) => {

    const textStyle = [styles.text]
    if(props.type === 'warning'){
        textStyle.push(styles.warning)
    }
    if(props.box === 'warning'){
        textStyle.push(styles.boxWarning)
    }

    if(props.type === 'success'){
        textStyle.push(styles.success)
    }
    if(props.box === 'success'){
        textStyle.push(styles.boxSucess)
    }
    return (
        <Text
            style={textStyle}>
            {props.children}
        </Text>
    )
}

export default TextContext
