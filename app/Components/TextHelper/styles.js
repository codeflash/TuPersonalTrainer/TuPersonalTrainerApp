import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
    text: {
        fontSize: 18,
        width: '100%',
        marginHorizontal: 5,
        marginTop: 5
    },
    warning: {
        color: 'red'
    },
    boxWarning: {
        padding: 10,
        color: '$white',
        backgroundColor: 'rgba(255,0,0,0.5)',
        borderRadius: 4
    },
    success: {
        color: 'green'
    },
    boxSuccess: {
        padding: 10,
        color: '$white',
        backgroundColor: 'rgba(0,255,0,0.5)',
        borderRadius: 4
    }
})