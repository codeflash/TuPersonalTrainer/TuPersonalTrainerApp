const contants = {
  brandColor: '#2ecc71',
  contrastColor: '#ffffff',
  textColorButton: '#ffffff',
  titleTextColor: '#000',
  fontSizeNormal: 18, 
  fontSizeButton: 16,
  facebookColor: '#3b5998',
  fontSizeTitle: 25,
  FontSizeSubtitle: 20,
}
export default contants;
