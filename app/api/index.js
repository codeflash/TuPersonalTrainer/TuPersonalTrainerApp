import axios from 'axios'
export const BASE_URL = 'http://104.155.128.84/api';

export const api_login = (email, password) => {
    const url = BASE_URL + '/login';

    return axios.post(url, {
        correo: email,
        password: password
    })
}

export const api_register = (username, email, password, weight, tall, goal, years, bodyFat, genre) => {
    const url = BASE_URL + '/register';

    return axios.post(url, {
        correo: email,
        password: password,
        peso: parseFloat(weight),
        nombre: username,
        altura: parseFloat(tall),
        objetivo: goal,
        edad: years,
        porcentaje_grasa: parseInt(bodyFat),
        genero: genre
    })
}

export const api_me = (token) => {
    const url = BASE_URL + '/me';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}

export const api_tips = (token) => {
    const url = BASE_URL + '/me/tips';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}

export const api_nutritionalplan = (token) => {
    const url = BASE_URL + '/me/nutritionalplan/last';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}

export const api_trainingplan = (token) => {
    const url = BASE_URL + '/me/trainingplan/last';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}


export const api_last_weight = (token) => {
    const url = BASE_URL + '/me/weight/last';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}

export const api_weight = (token) => {
    const url = BASE_URL + '/me/weight';
    return axios.get(url, {

        headers: {
            'x-access-token': token
        }

    })
}

export const api_add_weight = (token, weight) => {
    const url = BASE_URL + '/me/weight';
    return axios.post(url, {peso: parseFloat(weight)} ,{

        headers: {
            'x-access-token': token
        }

    })
}