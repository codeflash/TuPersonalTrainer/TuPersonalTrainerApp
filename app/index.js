import React, {Component} from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Provider, connect} from 'react-redux';
import {addNavigationHelpers} from 'react-navigation'
import Navigator from './config/routes';
import store from './config/store';

EStyleSheet.build({
  $brandColor: '#2ecc71',
  $primaryOrange: '#D57A66',
  $primaryGreen: '#00BD9D',
  $primaryPurple: '#9E768F',

  $white: '#FFFFFF',
  $black: '#000000',
  $lightGray: '#F0F0F0',
  $border: '#E2E2E2',
  $inputText: '#797979',
  $darkText: '#343434'
});

/*const App = ({dispatch, nav}) => (<Navigator navigation={addNavigationHelpers({dispatch, state: nav})}/>);

const mapStateToProps = state => ({nav: state.nav});

const AppWithNavigationState = connect(mapStateToProps)(App)*/

export default () => (
  <Provider store={store}>
    <Navigator/>
  </Provider>
)