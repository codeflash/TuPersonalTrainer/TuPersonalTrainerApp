import React, {Component} from 'react';
import {DrawerNavigator, StackNavigator} from 'react-navigation'; // 1.0.0-beta.14
import EStyleSheet from 'react-native-extended-stylesheet';

import Home from '../screens/Home';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Main from '../screens/Main';
import Workout from '../screens/Workout';
import MealPlan from '../screens/MealPlan';
import DetailWorkout from '../screens/DetailWorkout';
import Exercises from '../screens/Exercise';
import DetailMealPlan from '../screens/DetailMealPlan';
import Profile from '../screens/Profile';
import RegisterGenre from '../screens/RegisterGenre';
import RegisterGoal from '../screens/RegisterGoal';
import RegisterYears from '../screens/RegisterYears';
import RegisterTall from '../screens/RegisterTall';
import RegisterWeight from '../screens/RegisterWeight';
import RegisterBodyFat from '../screens/RegisterBodyFat';
import Loading from '../screens/Loading';
import Splash from '../screens/Splash';
import Weight from '../screens/Weight';
import AddWeight from '../screens/AddWeight';
import EditProfile from '../screens/EditProfile';
import ConfigRM from '../screens/ConfigRM';

import SideMenu from '../Components/SideMenu';

const DashboardStack = DrawerNavigator({
  Main: {
    screen: Main,
    navigationOptions: {
      drawerLabel: 'Inicio'
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      drawerLabel: 'Perfil'
    }
  },
  Workouts: {
    screen: Workout,
    navigationOptions: {
      drawerLabel: 'Plan de entrenamiento'
    }
  },
  MealPlan: {
    screen: MealPlan,
    navigationOptions: {
      drawerLabel: 'Plan Nutricional'
    }
  }
}, {contentComponent: SideMenu});

const RootStack = StackNavigator({
  Splash: {
    screen: Splash,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      header: null
    }
  },
  Weight: {
    screen: Weight,
    navigationOptions: {
      header: null
    }
  },
  ConfigRM: {
    screen: ConfigRM,
    navigationOptions: {
      header: null
    }
  },
  Loading: {
    screen: Loading,
    navigationOptions: {
      header: null
    }
  },
  AddWeight: {
    screen: AddWeight,
    navigationOptions: {
      header: null
    }
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: {
      header: null
    }
  },
  WorkoutDetail: {
    screen: DetailWorkout,
    navigationOptions: {
      header: null
    }
  },
  WorkoutExercises: {
    screen: Exercises,
    navigationOptions: {
      header: null
    }
  },
  MealPlanDetail: {
    screen: DetailMealPlan,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      header: null
    }
  },
  RegisterGenre: {
    screen: RegisterGenre,
    navigationOptions: {
      header: null
    }
  },
  RegisterGoal: {
    screen: RegisterGoal,
    navigationOptions: {
      header: null
    }
  },
  RegisterYears: {
    screen: RegisterYears,
    navigationOptions: {
      header: null
    }
  },
  RegisterTall: {
    screen: RegisterTall,
    navigationOptions: {
      header: null
    }
  },
  RegisterWeight: {
    screen: RegisterWeight,
    navigationOptions: {
      header: null
    }
  },
  RegisterBodyFat: {
    screen: RegisterBodyFat,
    navigationOptions: {
      header: null
    }
  },

  Dashboard: {
    screen: DashboardStack,
    navigationOptions: {
      header: null
    }
  }
});

export default RootStack;
