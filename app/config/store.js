import {createStore, applyMiddleware, compose} from 'redux';
import reducer from '../reducers';
import { AsyncStorage } from 'react-native';
import {persistStore , autoRehydrate} from 'redux-persist'
import logger from 'redux-logger'
import thunk from 'redux-thunk'



const middleware = [thunk,  logger]
const store = createStore(reducer , compose( applyMiddleware(...middleware) ))
//persistStore(store, { storage: AsyncStorage, blacklist: ['nav']  })
export default store ;
