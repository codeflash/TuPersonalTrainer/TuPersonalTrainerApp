import React, {Component} from 'react';
import {Platform, ScrollView} from 'react-native';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import TallItem from '../Components/TallItem';
import {connect} from 'react-redux'
class RegisterTallScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }

    handlePressNext = () => {
        this
            .props
            .navigation
            .navigate('RegisterWeight');
    }

    render() {
        const {navigation} = this.props;
        return (

            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar
                    left='arrow-back'
                    title='Paso 4 de 6'
                    onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <TallItem onPressNext={this.handlePressNext}/>
                </ScrollView>
            </Container>
        )
    }
}

export default connect()(RegisterTallScreen)