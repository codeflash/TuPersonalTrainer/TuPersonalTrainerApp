import React, {Component} from 'react';
import {StyleSheet, View, Text, Platform, ScrollView} from 'react-native';

import {connect} from 'react-redux';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import {Button} from '../Components/Button';
import ImageBackground from '../Components/ImageBackground';
import Navbar from '../Components/Navbar';
import {login, changePassword, changeEmail} from '../actions/auth'
import {TextContext} from '../Components/TextHelper'
import {Input} from '../Components/Input'
import {Body} from '../Components/Body'

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmEmail: false
    }
  }

  moveScreen(route) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: route})]
    })
    this
      .props
      .navigation
      .dispatch(resetAction)
  }

  handleChangeEmail = (text) => {
    this
      .props
      .changeEmail(text)
    this.setState({
      confirmEmail: !(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(text))
    })
  }
  handleChangeTextPassword = (text) => {
    this
      .props
      .changePassword(text)
  }

  handlePress = () => {
    this
      .props
      .login(this.props.email, this.props.password, this.props.navigation);
    this
      .props
      .navigation
      .navigate('Loading')
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  render() {

    return (
      <Container backgroundColor='white'>
        <Navbar left='arrow-back' title='Acceder' onPressLeft={() => this.goBack()}/>
        <ScrollView>
          <Body>
            <Input
              autoFocus={true}
              keyboardType='email-address'
              placeholder='Ingresa tu correo'
              value={this.props.email}
              onChangeText={this.handleChangeEmail}
              error={this.state.confirmEmail}
              message='Debes ingresar un email correcto'/>

            <Input
              placeholder='Ingresa tu contraseña'
              secureTextEntry={true}
              value={this.props.password}
              onChangeText={this.handleChangeTextPassword}/>
            <Button
              disabled={this.state.confirmEmail || !this.props.email || !this.props.password}
              onPress={this.handlePress}
              text='Acceder'/>
            <Button
              backgroundColor={CONSTANTS.facebookColor}
            
              icon='logo-facebook'
              text='Conectar con Facebook'/> 
              {this.props.errorSignIn && <TextContext box='warning'>Usuario o contraseña invalidas</TextContext>}
          </Body>
        </ScrollView>

      </Container>

    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password) => {
      dispatch(login(email, password))
    },
    changePassword: (password) => {
      dispatch(changePassword(password))
    },
    changeEmail: (email) => {
      dispatch(changeEmail(email))
    }
  }
}

const mapStateToProps = (state) => {
  return {email: state.auth.email, password: state.auth.password, errorSignIn: state.auth.errorSignIn}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
