import React, {Component} from 'react';
import {ScrollView, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import YearsItem from '../Components/YearsItem';
import {connect} from 'react-redux'


class RegisterYearsScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }

    handlePressNext = () => {
        this
            .props
            .navigation
            .navigate('RegisterTall');
    }

    render() {

        return (
            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar
                    left='arrow-back'
                    title='Paso 3 de 6'
                    onPressLeft={this.handleGoBack}/>
                
                    <YearsItem onPressNext={this.handlePressNext}/>
                
            </Container>
        )
    }
}


export default connect()(RegisterYearsScreen);