import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ScrollView,
    Platform
} from 'react-native';
import {changeUsername, changePassword, changeEmail, changePasswordConfirm, register} from '../actions/auth'
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import {Button} from '../Components/Button';
import ImageBackground from '../Components/ImageBackground';
import Navbar from '../Components/Navbar';
import {Title, Subtitle} from '../Components/Text';
import {Body} from '../Components/Body';
import {TextContext} from '../Components/TextHelper';
import {Input} from '../Components/Input'

class ConfigRMScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

            confirmEmail: false
        }
    }

    goBack() {
        this
            .props
            .navigation
            .goBack();
    }

    render() {

        return (
            <Container backgroundColor='white'>
                <Navbar
                    left='arrow-back'
                    title='Configuración 1RM'
                    onPressLeft={() => this.goBack()}/>
                <ScrollView>
                    <Body>
                        <Title alignCenter>Configuración 1RM</Title>
                        <View style={styles.separator}/>
                        <Subtitle >Sentadilla</Subtitle>
                        <Input
                            keyboardType='numeric'
                            autoFocus={true}
                            placeholder='50kg'
                            underlineColorAndroid={CONSTANTS.brandColor}/>
                        <Subtitle >Press de banca</Subtitle>
                        <Input
                            keyboardType='numeric'
                            placeholder='50kg'
                            underlineColorAndroid={CONSTANTS.brandColor}/>
                        <Subtitle >Peso Muerto</Subtitle>
                        <Input
                            keyboardType='numeric'
                            placeholder='50kg'
                            underlineColorAndroid={CONSTANTS.brandColor}/>
                        <Subtitle >Jalon al pecho</Subtitle>
                        <Input
                            keyboardType='numeric'
                            placeholder='50kg'
                            underlineColorAndroid={CONSTANTS.brandColor}/>
                        <View style={styles.separator}/>
                        <Button text='Guardar'/>
                    </Body>
                </ScrollView>

            </Container>
        );
    }
}


export default connect()(ConfigRMScreen);

const styles = StyleSheet.create({
    text: {
        marginVertical: 20,
        fontWeight: 'bold'
    },
    separator: {
        height: 10
    },

    inputText: {
        paddingVertical: 10,
        fontSize: 20,
        paddingVertical: 20,
        marginTop: 10
    }
});