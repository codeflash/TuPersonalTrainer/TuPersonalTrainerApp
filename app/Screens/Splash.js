import React, {Component} from 'react';
import {StyleSheet, View, ActivityIndicator, Text, AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import {Logo} from '../Components/Logo'
import {me} from '../actions/profile'

class SplashScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            token: ''
        }
    }
    async getToken(callback) {
        try {
            const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
            if (accessToken) {
                console.log('token', accessToken)
                this.setState({token: accessToken})
                this.props.me(accessToken);
            } else {
                console.log('error')
            }
        } catch (error) {
            console.log('error')
        }
    }
    moveScreen(route) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: route})]
        })
        this
            .props
            .navigation
            .dispatch(resetAction)
    }

    componentWillMount() {
        this.getToken();
        // Start counting when the page is loaded
        this.timeoutHandle = setTimeout(() => {
            
            //console.log(this.props.shouldRedirect)
            
            if (this.state.token) {
                this.moveScreen('Dashboard')
            } else {
                this.moveScreen('Home')
            }
        }, 5000);
       
        
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
    }
    render() {
        return (
            <View
                style={{
                flex: 1,
                backgroundColor: '#2ecc71',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Logo/>
                <Text
                    style={{
                    color: 'white',
                    marginTop: 20,
                    fontSize: 20
                }}>Cargando.....</Text>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      me: (token) => {
        dispatch(me(token))
      }
    }
  }

export default connect(null,mapDispatchToProps )(SplashScreen)