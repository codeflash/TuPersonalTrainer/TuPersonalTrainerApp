import React, {Component} from 'react';
import {
    ScrollView,
    Platform,
} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import GoalGroup from '../Components/GoalGroup';
import {connect} from 'react-redux'
import { changeGoal } from '../actions/auth'


class RegisterGoalScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }
    handlePressGainMuscleMass = () => {
        this.props.changeGoal('masa')
        this.props.navigation.navigate('RegisterYears')
    }
    handlePressLoseWeight = () => {
        this.props.changeGoal('peso')
        this.props.navigation.navigate('RegisterYears')
    }
    handlePressStayInshape = () => {
        this.props.changeGoal('forma')
        this.props.navigation.navigate('RegisterYears')
    }



    render() {
        return (
            <Container style={{backgroundColor: 'white'}}>
                <Navbar
                    left='arrow-back'
                    title='Paso 2 de 6'
                    onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <GoalGroup 
                        onPressGainMuscleMass={this.handlePressGainMuscleMass}
                        onPressLoseWeight={this.handlePressLoseWeight}
                        onPressStayInshape={this.handlePressStayInshape}
                    />
                </ScrollView>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeGoal: (goal) => {
            dispatch(changeGoal(goal));
        }
    }
}

export default connect(null, mapDispatchToProps)(RegisterGoalScreen);