import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  ScrollView,
  Platform
} from 'react-native';

//import Weight from './Weight';
import Icon from 'react-native-vector-icons/Ionicons';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import ItemList from '../Components/ItemList';
import {Title, Paragraph} from '../Components/Text';

export default class DetailMealPlanScreen extends Component {
  constructor(props) {
    super(props);
  }

  linkPressed(route) {
    this
      .props
      .navigation
      .navigate(route);
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  render() {
    const {state} = this.props.navigation;
    return (
      <Container>
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='arrow-back'
          title='Ejercicios'
          onPressLeft={() => this.goBack()}/>
        <ScrollView>

          <Title marginVertical marginHorizontal>Desayuno</Title>
          <View style={styles.containerText}>
            <Paragraph listItem={true}>
              2 huevos hervidos
            </Paragraph>
            <Paragraph listItem={true}>
              Ensalada de verduras
            </Paragraph>
            <Paragraph listItem={true}>
              Juego de papaya
            </Paragraph>
          </View>
          <Title marginVertical marginHorizontal>Almuerzo</Title>
          <View style={styles.containerText}>
            <Paragraph listItem={true}>
              Estofado
            </Paragraph>
            <Paragraph listItem={true}>
              Ensalada de verduras
            </Paragraph>
          </View>
          <Title marginVertical marginHorizontal>Cena</Title>
          <View style={styles.containerText}>
            <Paragraph listItem={true}>
              Sopa de pollo
            </Paragraph>
            <Paragraph listItem={true}>
              Ensalada de verduras
            </Paragraph>
          </View>

        </ScrollView>

      </Container>

    );
  }
}

const styles = StyleSheet.create({

  containerText: {
    padding: 20,
    backgroundColor: 'white'
  }

})
