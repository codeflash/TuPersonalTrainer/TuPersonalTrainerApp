import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  Platform,
  ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import ItemDetailWorkout from '../Components/ItemDetailWorkout';
import Tile from '../Components/Tile';
import {Title} from '../Components/Text';
import DetailWorkoutGroup from '../Components/DetailWorkoutGroup';

export default class DetailWorkoutScreen extends Component {
  constructor(props) {
    super(props);
  }

  linkPressed(route) {
    this
      .props
      .navigation
      .navigate(route);
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  render() {
    const screenName = 'Ejercicios';
    return (
      <Container style={{
        backgroundColor: 'white'
      }}>
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='arrow-back'
          title={screenName}
          onPressLeft={() => this.goBack()}/>
        <ScrollView>
          <ItemDetailWorkout
            exercise='Sentadillas'
            onPress={() => this.linkPressed('WorkoutExercises')}/>
          <ItemDetailWorkout
            exercise='Press de banca'
            onPress={() => this.linkPressed('WorkoutExercises')}/>
          <ItemDetailWorkout
            exercise='Remo'
            onPress={() => this.linkPressed('WorkoutExercises')}/>
          <ItemDetailWorkout
            exercise='Dominadas'
            onPress={() => this.linkPressed('WorkoutExercises')}/>
        </ScrollView>
      </Container>

    );
  }
}

const styles = StyleSheet.create({

  line: {
    height: 2,
    backgroundColor: '#ccc'
  },

  text: {
    fontSize: CONSTANTS.fontButton,
    color: CONSTANTS.textColorButton
  },
  containerExercise: {
    padding: 20
  }

})
