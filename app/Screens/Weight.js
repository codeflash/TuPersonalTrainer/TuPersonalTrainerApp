import React, {Component} from 'react'
import {
    View,
    ScrollView,
    Text,
    Dimensions,
    AsyncStorage,
    ActivityIndicator
} from 'react-native'
import Container from '../Components/Container'
import {Body} from '../Components/Body'
import {connect} from 'react-redux'
import CONSTANTS from '../Constants'
import Navbar from '../Components/Navbar'
import Card from '../Components/Card'
import {Button} from '../Components/Button'
import {Title, Subtitle} from '../Components/Text'
import EStyleSheet from 'react-native-extended-stylesheet'
import {weight} from '../actions/weight'
import {VictoryChart, VictoryLine, VictoryTheme} from 'victory-native'
import {NotLoad} from '../Components/NotLoad'

const {width, height} = Dimensions.get('window');
class WeightScreen extends Component {
    constructor(props) {
        super(props)
    }

    async getToken() {
        try {
            const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
            if (accessToken) {
                this
                    .props
                    .weight(accessToken);

            } else {
                console.log('error')
            }
        } catch (error) {
            console.log('error')
        }
    }

    handlePressLoad = () => {
        this.getToken()
    } 

    componentWillReceiveProps(nextProps){
        if(this.props.weights.length > 0 && this.props.weights.length !== nextProps.weights.length){
            this.getToken()
        }
        console.log('propiedades' , this.props)
        console.log('siguientesPropiedades', nextProps)
    }

    componentWillMount() {
        this.getToken()
    }

    handleBack = () => {
        this
            .props
            .navigation
            .goBack();
    }
    handleAddWeight = () => {
        this
            .props
            .navigation
            .navigate('AddWeight');
    }
    render() {
        return (
            <Container>
                <Navbar
                    backgroundColor={CONSTANTS.brandColor}
                    color={CONSTANTS.textColorButton}
                    title='Peso'
                    left='arrow-back'
                    right='add'
                    onPressRight={this.handleAddWeight}
                    onPressLeft={this.handleBack}/> 
                    {(!this.props.isFetching && !this.props.error)
                    ? <ScrollView>
                            <Body>
                                <Card>
                                    <View style={styles.circleContainer}>
                                        <Title>{this.props.weights.length > 0 && this.props.weights[this.props.weights.length - 1].y + ' Kg'}</Title>
                                        <Subtitle>Peso Actual</Subtitle>
                                    </View>
                                </Card>
                                <View style={styles.separator}/>
                                <Card>

                                    <VictoryChart width={width - 64}>
                                        <VictoryLine
                                            style={{
                                            data: {
                                                stroke: "#2ecc71"
                                            },
                                            parent: {
                                                border: "1px solid #ccc"
                                            }
                                        }}
                                            domain={{
                                            x: [
                                                1, 31
                                            ],
                                            y: [20, 299]
                                        }}
                                            data={(this.props.weights.length > 1)
                                            ? this.props.weights
                                            : [
                                                {
                                                    y: 60,
                                                    x: 8
                                                }, {
                                                    y: 80,
                                                    x: 9
                                                }
                                            ]}/>

                                    </VictoryChart>

                                </Card>
                            </Body>
                        </ScrollView>
                    : (this.props.error && !this.props.isFetching)
                        ? <NotLoad onPress={this.handlePressLoad}/>
                        : <Body center><ActivityIndicator size='large'/></Body>
                    }
            </Container>
        )
    }
}

const styles = EStyleSheet.create({
    separator: {
        height: 10
    },
    circleDescription: {
        marginTop: 10,
        fontSize: 16
    },
    circleContainer: {
        paddingVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        width: 70,
        height: 70,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 35,
        backgroundColor: '$brandColor'
    },
    circleText: {
        fontSize: 20,
        color: '$white'
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        weight: (token) => {
            dispatch(weight(token))
        }
    }
}

const mapStateToProps = (state) => {
    return {isFetching: state.weight.isFetching, weights: state.weight.data, error: state.weight.error}
}

export default connect(mapStateToProps, mapDispatchToProps)(WeightScreen)
