import React, {Component} from 'react';
import {StyleSheet, View, ActivityIndicator, Text , AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';

class LoadingScreen extends Component {

    moveScreen(route) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: route})]
        })
        this
            .props
            .navigation
            .dispatch(resetAction)
    }

    componentWillMount() {
        // Start counting when the page is loaded
        this.timeoutHandle = setTimeout(() => {
            //console.log(this.props.shouldRedirect)
            if (this.props.shouldRedirect) {
                this.storeToken(this.props.token)
                this.moveScreen('Dashboard')
            } else {
                this
                    .props
                    .navigation
                    .goBack()
            }
        }, 5000);
    }

    storeToken(token) {
        AsyncStorage.setItem('ACCESS_TOKEN', token, (err) => {
            if (err) {
                throw err;
            }
        }).catch((err) => {
            throw err;
        });
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
    }
    render() {
        return (
            <View
                style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <ActivityIndicator size='large'/>
                <Text
                    style={{
                    marginTop: 20,
                    fontSize: 20
                }}>Cargando.....</Text>
            </View>
        )
    }
}



const mapStateToProps = (state) => {
    return {shouldRedirect: state.auth.shouldRedirect, token: state.auth.token}
}

export default connect(mapStateToProps)(LoadingScreen)