import React, {Component} from 'react';
import {ScrollView, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import WeightItem from '../Components/WeightItem';
import {connect} from 'react-redux'

class RegisterWeightScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }
    handlePressNext = () => {
        this
            .props
            .navigation
            .navigate('RegisterBodyFat');
    }

    render() {
        const {navigation} = this.props;
        return (
            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar
                    left='arrow-back'
                    title='Paso 5 de 6'
                    onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <WeightItem onPressNext={this.handlePressNext}/>
                </ScrollView>
            </Container>
        )
    }
}

export default connect()(RegisterWeightScreen)