import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  Platform,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  FlatList
} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import ItemWorkout from '../Components/ItemWorkout';
import Tile from '../Components/Tile';
import {connect} from 'react-redux'
import {Title} from '../Components/Text';
import {getTrainingPlan} from '../actions/trainingplan';
import WorkoutGroup from '../Components/WorkoutGroup';
import {NotLoad} from '../Components/NotLoad'
import {Body} from '../Components/Body'

class WorkoutScreen extends Component {
  constructor(props) {
    super(props);
  }

  async getToken() {
    try {
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (accessToken) {
        this
          .props
          .getTrainingPlan(accessToken);

      } else {
        console.log('error')
      }
    } catch (error) {
      console.log('error')
    }
  }

  handlePressLoad = () => {
    this.getToken()
  }
  componentWillMount() {
    this.getToken()
  }

  render() {
    const screenName = 'Plan de entrenamiento'
    const {navigation} = this.props;
    return (
      <Container style={{
        backgroundColor: 'white'
      }}>
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='menu'
          title={screenName}
          right='settings'
          onPressLeft={() => navigation.navigate('DrawerToggle')}
          onPressRight={() => navigation.navigate('ConfigRM')}/> 
          {(!this.props.isFetching && !this.props.error)
          ? <FlatList
              keyExtractor={this.keyExtractor}
              data={this.props.trainingplan.dia}
              renderItem={({item, key}) => <ItemWorkout
              key={key}
              day={item.nro_dia}
              onPress={() => navigation.navigate('WorkoutDetail', {ejercicios: item.ejercicio})}/>}/>
          : (this.props.error && !this.props.isFetching)
            ? <NotLoad onPress={this.handlePressLoad}/>
            : <Body center><ActivityIndicator size='large'/></Body>
}
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTrainingPlan: (token) => {
      dispatch(getTrainingPlan(token))
    }
  }
}

const mapStateToProps = (state) => {
  return {isFetching: state.trainingplan.isFetching, trainingplan: state.trainingplan.data, error: state.trainingplan.error}
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkoutScreen)