import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  Platform
} from 'react-native';
import {changeUsername, changePassword, changeEmail, changePasswordConfirm, register} from '../actions/auth'
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import {Button} from '../Components/Button';
import ImageBackground from '../Components/ImageBackground';
import Navbar from '../Components/Navbar';
import {Body} from '../Components/Body';
import {TextContext} from '../Components/TextHelper';
import {Input} from '../Components/Input'

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      confirmEmail: false,
    }
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  handlePressLogin = () => {
    this.props.register(
      this.props.username,
      this.props.email,
      this.props.password,
      this.props.weight,
      this.props.tall,
      this.props.goal,
      this.props.years,
      this.props.bodyFat,
      this.props.genre,
    )
    this.props.navigation.navigate('Loading')
  }

  handleChangePassword = (text) => {
    this
      .props
      .changePassword(text)

  }
  
  handleChangeEmail = (text) => {
    this
      .props
      .changeEmail(text)
    this.setState({
      confirmEmail: !(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(text))
    })
  }

  render() {

    return (
      <Container backgroundColor='white'>
        <Navbar
          left='arrow-back'
          title='Empezar ahora'
          onPressLeft={() => this.goBack()}/>
        <ScrollView>
          <Body>
            <Input
              autoFocus={true}
              placeholder='Nombre Completo'
              onChangeText={(username) => this.props.changeUsername(username)}
              underlineColorAndroid={CONSTANTS.brandColor}/>
            <Input

              style={styles.inputText}
              keyboardType='email-address'
              placeholder='Correo'
              onChangeText={this.handleChangeEmail}
              underlineColorAndroid={CONSTANTS.brandColor}
              error={this.state.confirmEmail}
              message='Ingresa un email valido'/> 
          
            <Input
              style={styles.inputText}
              placeholder='Contraseña'
              secureTextEntry={true}
              onChangeText={this.handleChangePassword}
              underlineColorAndroid={CONSTANTS.brandColor}/>
            <Button
              disabled={this.state.confirmEmail || !this.props.email || !this.props.password || !this.props.username}
              onPress={this.handlePressLogin}
              text='Registrarse'/>
            <Button
              backgroundColor={CONSTANTS.facebookColor}
              onPress={() => null}
              icon='logo-facebook'
              text='Conectar con Facebook'/>
            {this.props.errorSignUp && <TextContext box='warning'>Ocurrio un problema. Vuelve a intentarlo</TextContext>}
          </Body>
        </ScrollView>

      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {
    username: state.auth.username,
    email: state.auth.email,
    password: state.auth.password,
    passwordConfirm: state.auth.passwordConfirm,
    genre: state.auth.genre,
    bodyFat: state.auth.bodyFat,
    years: state.auth.years,
    goal: state.auth.goal,
    weight: state.auth.weight,
    tall: state.auth.tall,
    errorSignUp: state.auth.errorSignUp
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    register: (username, email, password ,weight , tall , goal, years , bodyFat) => {
      dispatch(register(username, email, password ,weight , tall , goal, years, bodyFat))
    },
    changePassword: (password) => {
      dispatch(changePassword(password))
    },
    changePasswordConfirm: (passwordConfirm) => {
      dispatch(changePasswordConfirm(passwordConfirm))
    },
    changeUsername: (username) => {
      dispatch(changeUsername(username))
    },
    changeEmail: (email) => {
      dispatch(changeEmail(email))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

const styles = StyleSheet.create({
  text: {
    marginVertical: 20,
    fontWeight: 'bold'
  },

  inputText: {
    paddingVertical: 10,
    fontSize: 20,
    paddingVertical: 20,
    marginTop: 10
  }
});