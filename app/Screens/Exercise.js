import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  ScrollView,
  Platform
} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import Line from '../Components/Line';
import {Button} from '../Components/Button';
import {Body} from '../Components/Body';
import {Paragraph, Title, Subtitle, SubtitleAndText} from '../Components/Text';

export default class ExerciseScreen extends Component {
  constructor(props) {
    super(props);
  }

  linkPressed(route) {
    this
      .props
      .navigation
      .navigate(route);
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  render() {

    return (
      <Container>
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='arrow-back'
          title='Sentadilla'
          onPressLeft={() => this.goBack()}/>
        <ScrollView>
          <Image
            style={styles.video}
            source={require('../../assets/images/sentadilla.jpg')}/>

          <Title text='Sentadilla' marginVertical={true} marginHorizontal={true}/>
          <Line horizontal/>
          <Body>
            <SubtitleAndText marginVertical={true} left='Repeticiones' right='10'/>
            <SubtitleAndText marginVertical={true} left='Series' right='4'/>
            <SubtitleAndText marginVertical={true} left='Descanso' right='60 segundos'/>
            <SubtitleAndText marginVertical={true} left='Peso' right='5kg'/>
            <Subtitle marginVertical={true}>Musculos Implicados:</Subtitle>
            <Paragraph listItem={true}>
              Cuadriceps
            </Paragraph>
            <Paragraph listItem={true}>
              Femorales
            </Paragraph>
            <Subtitle marginVertical={true}>Asi es como cuenta una repetición:</Subtitle>
            <Paragraph listItemIcon='1.-' listItem={true}>
              Comienza de pie con los pies juntos
            </Paragraph>
            <Paragraph listItemIcon='2.-' listItem={true}>
              Coloca los brazos en cruz
            </Paragraph>
            <Paragraph listItemIcon='3.-' listItem={true}>
              Eleva y desciende los brazos unos centimetros a rápida velocidad
            </Paragraph>
            <Subtitle marginVertical={true}>Notas</Subtitle>
            <Paragraph listItem={true}>
              Manten una buena postura
            </Paragraph>
            <Paragraph listItem={true}>
              Manten los brazos paralelos al suelo
            </Paragraph>
          </Body>
        </ScrollView>
        <View style={styles.showVideoContainer}>
          <Button text='Ver Video'/>
        </View>

      </Container>

    );
  }
}

const styles = StyleSheet.create({
  showVideoContainer:{
    marginHorizontal: 16,
    marginBottom: 20
  },
  video: {
    flex: 1,
    width: null,
    resizeMode: 'cover',
    height: 250
  }
})
