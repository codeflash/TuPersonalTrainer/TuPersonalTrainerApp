import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  ScrollView,
  Platform,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import ImageButton from '../Components/ImageButton';
import ItemList from '../Components/ItemList';
import {ImageBackground} from '../Components/ImageBackground';
import {Title} from '../Components/Text';
import {Button} from '../Components/Button';
import {Body} from '../Components/Body';
import {connect} from 'react-redux';
import {me , getInitialState} from '../actions/profile'
import {NotLoad} from '../Components/NotLoad'
import {NavigationActions} from 'react-navigation';

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
  }
  async getToken() {
    try {
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (accessToken) {
        this
          .props
          .me(accessToken);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log('error')
    }
  }

  componentWillMount() {
    this.props.getInitialState();
    this.getToken()
  }

  handlePressLoad = () => {
    this.getToken()
  }

  moveScreen(route) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: route})]
    })
    this
      .props
      .navigation
      .dispatch(resetAction)
  }

  linkPressed(route) {
    this
      .props
      .navigation
      .navigate(route);
  }

  setGoalName = () => {
    if (this.props.data.objetivo === 'peso') {
      return 'Perder peso'
    } else if (this.props.data.objetivo === 'masa') {
      return 'Aumentar masa muscular'
    } else if (this.props.data.objetivo === 'forma') {
      return 'Mantenerse en forma'
    }
  }

  setGenre = () => {
    if (this.props.data.genero === 'm') {
      return 'Varon'
    } else if (this.props.data.genero === 'f') {
      return 'Mujer'
    }
  }

  render() {
    const {state} = this.props.navigation;
    return (
      <Container style={{
        backgroundColor: 'white'
      }}>
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='menu'
          title='Perfil'
          right='settings'
          onPressLeft={() => this.linkPressed('DrawerToggle')}
          onPressRight={() => this.linkPressed('EditProfile')}/>
          {(!this.props.isFetching && !this.props.error)
          ? <ScrollView>

              <View style={{
                backgroundColor: 'white'
              }}>
                <ImageBackground
                  center
                  source={{
                  uri: 'https://picsum.photos/100/100/?image=190'
                }}
                  style={styles.containerImageAndName}>
                  <ImageButton
                    style={{
                    marginVertical: 20
                  }}
                    height={150}
                    width={150}
                    circle={true}
                    onPress={() => null}
                    src={require('../../assets/images/profile.jpg')}/>
                  <Title
                    style={[
                    styles.nameProfile, {
                      color: 'white',
                      textShadowOffset: {
                        width: 2,
                        height: 2
                      },
                      textShadowRadius: 5,
                      textShadowColor: '#000'
                    }
                  ]}>{this.props.data.nombre}</Title>
                </ImageBackground>
                <ItemList left='Genero' right='Masculino'/>
                <ItemList left='Objetivo' right={this.setGoalName()}/>
                <ItemList left='Peso' right={this.props.data.peso + " Kg"}/>
                <ItemList left='Edad' right={this.props.data.edad + " años"}/>
                <ItemList left='Altura' right={this.props.data.altura + " cm"}/>
                <ItemList left='% de grasa' right={this.props.data.porcentaje_grasa + " %"}/>

              </View>

            </ScrollView>
          : (this.props.error && !this.props.isFetching)
            ? <NotLoad onPress={this.handlePressLoad}/>
            : <Body center><ActivityIndicator size='large'/></Body>}

      </Container>

    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    me: (token) => {
      dispatch(me(token))
    },
    getInitialState: () => {
      dispatch(getInitialState())
    }
  }
}

const mapStateToProps = (state) => {
  return {data: state.profile.data, isFetching: state.profile.isFetching, error: state.profile.error}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);

const styles = StyleSheet.create({

  containerDays: {
    flexWrap: 'wrap',
    flexDirection: 'row'
  },

  containerImageAndName: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  nameProfile: {
    fontSize: CONSTANTS.fontSizeTitle,
    marginBottom: 20
  }
})
