import React, {Component} from 'react';
import {View, ScrollView, AsyncStorage, ActivityIndicator} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import WeightBox from '../Components/WeightBox';
import WorkoutBox from '../Components/WorkoutBox';
import TipsBox from '../Components/TipsBox';
import {Button} from '../Components/Button';
import Tile from '../Components/Tile';
import {Paragraph, Title} from '../Components/Text';
import {Body} from '../Components/Body';
import {HeaderMain} from '../Components/Header';
import {connect} from 'react-redux';
import {tips} from '../actions/tips'
import {weight} from '../actions/weight'
import {NotLoad} from '../Components/NotLoad'

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentWorkout: ''
    }
  }

  async getToken() {
    try {
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (accessToken) {
        this
          .props
          .getTips(accessToken);
        this
          .props
          .weight(accessToken);
      } else {
        console.log('error get token')
      }
    } catch (error) {
      console.log('error get token')
    }
  }

  componentWillMount() {
    this.getToken()
    const exercise = {
      id: 1,
      name: 'Sentadilla',
      repetitions: 15,
      sets: 4,
      load: 10,
      break: 60
    }
    const currentWorkout = Array(5).fill(exercise);

    this.setState({currentWorkout: currentWorkout})
  }

  handlePressWeight = () => {
    this
      .props
      .navigation
      .navigate('Weight');
  }

  render() {
    const {navigation} = this.props;

    return (

      <Container>

        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          title='MetasFit'
          left='menu'
          onPressLeft={() => navigation.navigate('DrawerToggle')}/> 
          {(!this.props.isFetchingTips && !this.props.isFetchingWeights && !this.props.errorTips && !this.props.errorWeights)
          ? <ScrollView >
              <View style={{
                paddingBottom: 10
              }}>
                <HeaderMain title='MetasFit' tips={this.props.tips}/>
                <WeightBox
                  currentWeight={this.props.weights.length > 0 && this.props.weights[this.props.weights.length - 1].y}
                  onPress={this.handlePressWeight}/>
                <WorkoutBox
                  currentWorkout={this.state.currentWorkout}
                  onPress={() => navigation.navigate('WorkoutExercises')}/>
              </View>
            </ScrollView>
          : (this.props.errorWeights && this.props.errorTips && !this.props.isFetchingWeights && !this.props.isFetchingTips)
            ? <NotLoad onPress={this.handlePressLoad}/>
            : <Body center><ActivityIndicator size='large'/></Body>}

      </Container>

    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getTips: (token) => {
      dispatch(tips(token))
    },
    weight: (token) => {
      dispatch(weight(token))
    }
  }
}

const mapStateToProps = (state) => {
  return {
    tips: state.tips.data,
    isFetchingTips: state.tips.isFetching,
    errorTips: state.tips.error,
    isFetchingWeights: state.weight.isFetching,
    weights: state.weight.data,
    errorWeights: state.weight.error
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
