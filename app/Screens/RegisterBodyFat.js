import React, {Component} from 'react';
import {ScrollView, Platform} from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import {BodyFat} from '../Components/BodyFat';
import {Body} from '../Components/Body'
import {connect} from 'react-redux'

class RegisterBodyFatScreen extends Component {

    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }

    handlePressNext = () => {
        this
            .props
            .navigation
            .navigate('Register');
    }

    render() {
        return (
            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar
                    left='arrow-back'
                    title='Paso 6 de 6'
                    onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <BodyFat onPressNext={this.handlePressNext}/>
                </ScrollView>
            </Container>
        )
    }
}

export default connect()(RegisterBodyFatScreen)
