import React, {Component} from 'react';
import {ScrollView, ActivityIndicator} from 'react-native';
import Container from '../Components/Container';
import {BackgroundHome} from '../Components/ImageBackground';
import {Button} from '../Components/Button';
import {LogoWithDescription} from '../Components/Logo'
import {Body} from '../Components/Body'
import Orientation from 'react-native-orientation';
import {connect} from 'react-redux'
import {getInitialState} from '../actions/auth'

class HomeScreen extends Component {

  constructor(props) {
    super(props)
  }
  componentDidMount() {
    this.props.getInitialState();
    Orientation.lockToPortrait()
    
  }

 

  handlePressRegister = () => {
    this.props.getInitialState();
    this
      .props
      .navigation
      .navigate('RegisterGenre')
  }

  handlePressLogin = () => {
    this.props.getInitialState();
    this
      .props
      .navigation
      .navigate('Login')
  }

  render() {
    
    return (
      <Container>
        <BackgroundHome>

          <Body center>
            <LogoWithDescription/>
            <Button onPress={this.handlePressRegister} text='Empieza ahora'/>
            <Button
              backgroundColor={'transparent'}
              onPress={this.handlePressLogin}
              text='Ya tengo cuenta'/>
          </Body>

        </BackgroundHome>
      </Container>

    )
   
  }

}

const mapStateToProps = state => {

  return {shouldRedirect: state.auth.shouldRedirect}
}

const mapDispatchToProps = (dispatch) => {
  return {
    getInitialState: () => {
      dispatch(getInitialState())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
