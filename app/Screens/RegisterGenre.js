import React, {Component} from 'react';
import {ScrollView, Platform} from 'react-native';

import {connect} from 'react-redux'
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import GenreGroup from '../Components/GenreGroup';
import {changeGenre} from '../actions/auth'

class RegisterGenreScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }
    handleOnPressWoman = () => {
        this
            .props
            .changeGenre('f')
        this
            .props
            .navigation
            .navigate('RegisterGoal')

    }
    handleOnPressMan = () => {
        this
            .props
            .changeGenre('m')
        this
            .props
            .navigation
            .navigate('RegisterGoal')
    }
    render() {
        return (
            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar left='arrow-back' title='Paso 1 de 6' onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <GenreGroup
                        onPressWoman={this.handleOnPressWoman}
                        onPressMan={this.handleOnPressMan}/>
                </ScrollView>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeGenre: (genre) => {
            dispatch(changeGenre(genre));
        }
    }
}

export default connect(null, mapDispatchToProps)(RegisterGenreScreen);