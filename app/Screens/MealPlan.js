import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  Dimensions,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import Tile from '../Components/Tile';
import {Title} from '../Components/Text';
import {getNutritionalPlan} from '../actions/nutritionalplan'
import {NotLoad} from '../Components/NotLoad'
import {Body} from '../Components/Body'
import {connect} from 'react-redux';
const {width, height} = Dimensions.get('window');

class MealPlanScreen extends Component {
  constructor(props) {
    super(props);
  }

  linkPressed(route, object) {
    this
      .props
      .navigation
      .navigate(route, object);
  }

  async getToken() {
    try {
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (accessToken) {       
        this.props.getNutritionalPlan(accessToken);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log('error')
    }
  }
  componentWillMount() {
    this.getToken()  
  }

  handlePressLoad  = () => {
    this.getToken()  
  }

  getDays = () => {
    let days = []
    this
      .props
      .nutritionalplan
      .forEach((day) => {
        days.push(
          <TouchableOpacity
            activeOpacity={0.7}
            key={i}
            style={[
            styles.daybox, {
              backgroundColor: (i <= 20)
                ? CONSTANTS.brandColor
                : 'transparent',
              borderColor: (i <= 20)
                ? 'white'
                : 'gray'
            }
          ]}
            onPress={() => this.linkPressed('MealPlanDetail', {
            day: day
          })}>
            <Text
              style={[
              styles.textDay, {
                color: (i <= 20)
                  ? 'white'
                  : 'gray'
              }
            ]}>{day.dia - 1}</Text>
          </TouchableOpacity>
        )
      })
  }

  render() {
    const screenName = 'Plan Nutricional';
    return (
      <Container >
        <Navbar
          backgroundColor={CONSTANTS.brandColor}
          color={CONSTANTS.textColorButton}
          left='menu'
          title={screenName}
          onPressLeft={() => this.linkPressed('DrawerToggle')}/> 
          {(!this.props.isFetching && !this.props.error)
          ? <ScrollView style={{
              backgroundColor: 'white'
            }}>
              <View style={styles.containerDays}>
                {this.getDays()}
              </View>
            </ScrollView>
          : (this.props.error && !this.props.isFetching)
            ? <NotLoad onPress={this.handlePressLoad}/>
            : <Body center><ActivityIndicator size='large'/></Body>}
      </Container>

    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getNutritionalPlan: (token) => {
      dispatch(getNutritionalPlan(token))
    }
  }
}

const mapStateToProps = (state) => {
  return {isFetching: state.nutritionalplan.isFetching, nutritionalplan: state.nutritionalplan.data, error: state.nutritionalplan.error}
}

export default connect(mapStateToProps, mapDispatchToProps)(MealPlanScreen)

const widthCircle = (width - 32 - 12 * 2) / 6;
const styles = StyleSheet.create({

  containerDays: {
    paddingHorizontal: 16,
    paddingVertical: 20,
    flexWrap: 'wrap',
    flexDirection: 'row'
  },

  text: {
    fontSize: CONSTANTS.fontButton,
    color: CONSTANTS.textColorButton
  },

  daybox: {
    width: widthCircle,
    marginHorizontal: 2,
    marginVertical: 4,
    borderWidth: 2,
    height: widthCircle,
    borderRadius: widthCircle / 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textDay: {
    fontSize: 25
  }

})
