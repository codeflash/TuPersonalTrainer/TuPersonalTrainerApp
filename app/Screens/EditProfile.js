import React, {Component} from 'react';
import {StyleSheet, View, Text, Platform, ScrollView, AsyncStorage,
  ActivityIndicator} from 'react-native';

import {connect} from 'react-redux';
import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import {Button} from '../Components/Button';
import ImageBackground from '../Components/ImageBackground';
import Navbar from '../Components/Navbar';

import {TextContext} from '../Components/TextHelper'
import {NavigationActions} from 'react-navigation'
import {Input} from '../Components/Input'
import {Body} from '../Components/Body'
import {me, changePassword, changeEmail, changeUsername} from '../actions/profile'
import {NotLoad} from '../Components/NotLoad'
import {getInitialState} from '../actions/auth'

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmEmail: false
    }
  }

  moveScreen(route) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: route})]
    })
    this
      .props
      .navigation
      .dispatch(resetAction)
  }

  async deleteToken() {
    try {
      await AsyncStorage.removeItem('ACCESS_TOKEN')
      this.moveScreen('Home');
    } catch (error) {
      console.warn(error.message);
    }
  }

  async getToken() {
    try {
      const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (accessToken) {       
        this.props.me(accessToken);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log('error')
    }
  }

  componentWillMount() {
    this.getToken()  
  }

  handlePressLoad  = () => {
    this.getToken()  
  }

  handleDelete = () => {
    this.props.getInitialState();
    this.deleteToken();
  }

  handleChangeEmail = (text) => {
    this
      .props
      .changeEmail(text)
    this.setState({
      confirmEmail: !(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(text))
    })
  }
  handleChangeTextPassword = (text) => {
    this
      .props
      .changePassword(text)
  }

  handlePress = () => {
    this
      .props
      .login(this.props.email, this.props.password, this.props.navigation);
    
  }

  goBack() {
    this
      .props
      .navigation
      .goBack();
  }

  render() {

    return (
      <Container backgroundColor='white'>
        <Navbar left='arrow-back' title='Editar Perfil' onPressLeft={() => this.goBack()}/>
        
        {(!this.props.isFetching && !this.props.error) ? 
          <ScrollView>
          <Body>
            <Input
              autoFocus={true}
              placeholder='Nombre Completo'
              value={this.props.username}
              onChangeText={(username) => this.props.changeUsername(username)}
              underlineColorAndroid={CONSTANTS.brandColor}/>
            <Input
              keyboardType='email-address'
              placeholder='Ingresa tu correo'
              value={this.props.email}
              onChangeText={this.handleChangeEmail}
              error={this.state.confirmEmail}
              message='Debes ingresar un email correcto'/>

            <Input
              placeholder='Ingresa tu contraseña'
              secureTextEntry={true}
              value={this.props.password}
              onChangeText={this.handleChangeTextPassword}/>
            <Button
              disabled={this.state.confirmEmail || !this.props.email || !this.props.password || !this.props.username}
              onPress={this.handlePress}
              text='Guardar'/>
            <Button type='warning' onPress={this.handleDelete} text='Cerrar Sesión'/>
        
            {this.props.errorUpdate && <TextContext box='warning'>Ocurrio un problema. Vuelve a intentarlo</TextContext>}
          </Body>
        </ScrollView>
      : (this.props.error && !this.props.isFetching) ? <NotLoad onPress={this.handlePressLoad}/> : <Body center><ActivityIndicator size='large'/></Body> }

      </Container>

    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    me: (token) => {
      dispatch(me(token))
    },
    changePassword: (password) => {
      dispatch(changePassword(password))
    },
    changeUsername: (username) => {
      dispatch(changeUsername(username))
    },
    changeEmail: (email) => {
      dispatch(changeEmail(email))
    },
    getInitialState: () => {
      dispatch(getInitialState())
    }
  }
}

const mapStateToProps = (state) => {
  return {isFetching: state.profile.isFetching, username: state.profile.username, email: state.profile.email, password: state.profile.password, error: state.profile.error}
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
