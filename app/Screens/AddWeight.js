import React, {Component} from 'react';
import {ScrollView, Platform,AsyncStorage } from 'react-native';

import CONSTANTS from '../Constants';
import Container from '../Components/Container';
import Navbar from '../Components/Navbar';
import WeightItem from '../Components/WeightItem';
import {connect} from 'react-redux'
import {add_weight} from '../actions/weight'
import {TextContext} from '../Components/TextHelper'
import {Body} from '../Components/Body'
class RegisterWeightScreen extends Component {
    handleGoBack = () => {
        this
            .props
            .navigation
            .goBack();
    }

    async getToken() {
        try {
            const accessToken = await AsyncStorage.getItem('ACCESS_TOKEN');
            if (accessToken) {
                this
                    .props
                    .add_weight(accessToken , this.props.weight, this.props.navigation);
            } else {
                console.log('error get token')
            }
        } catch (error) {
            console.log('error get token')
        }
    }

    handlePressNext = () => {
        this.getToken();
        
    }

    render() {
        const {navigation} = this.props;
        return (
            <Container style={{
                backgroundColor: 'white'
            }}>
                <Navbar left='arrow-back' title='Agregar Peso' onPressLeft={this.handleGoBack}/>
                <ScrollView>
                    <WeightItem textButton='Guardar' onPressNext={this.handlePressNext} isFetching={this.props.isFetching}/>
                    <Body>
                        {this.props.error && <TextContext box='warning'>Ocurrio un error</TextContext>}
                    </Body>
                </ScrollView>
            </Container>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add_weight: (token, weight, navigation) => {
            dispatch(add_weight(token, weight , navigation))
        }
    }
}

const mapStateToProps = (state) => {
    return {isFetching: state.weight.isFetching, error: state.weight.error, weight: state.auth.weight}
}

export default connect(mapStateToProps,mapDispatchToProps)(RegisterWeightScreen)