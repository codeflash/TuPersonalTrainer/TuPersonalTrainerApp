import { NavigationActions } from 'react-navigation';
import  AppNavigator  from '../config/routes';
import {LOGIN_USER, LOGOUT_USER} from '../actions/auth'
// Start with two routes: The Main screen, with the Login screen on top.


export default (state, action) => {
  let nextState;
  switch (action.type) {
    case LOGIN_USER:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Dashboard' }),
        state
      );
      break;
    case LOGOUT_USER:
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Home' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
