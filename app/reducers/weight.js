import {WEIGHT_FETCHING_DATA, WEIGHT_FETCHING_DATA_SUCCESS, WEIGHT_FETCHING_DATA_FAILURE, WEIGHT_CREATE_DATA_SUCCESS} from '../actions/weight'

const defaultState = {
    data: [],
    error: false,
    isFetching: false,
    success: false
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case WEIGHT_CREATE_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: state.data.concat({y: parseFloat(action.item), x: 0}),
                isFetching: false,
                success: true
            };
        case WEIGHT_FETCHING_DATA:
            return {
                ...state,
                error: false,
                isFetching: true
            };
        case WEIGHT_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: action.data,
                isFetching: false,
                success: true
            };
        case WEIGHT_FETCHING_DATA_FAILURE:
            return {
                ...state,
                error: true,
                isFetching: false,
                success: false
            };
        default:
            return state;
    }
}