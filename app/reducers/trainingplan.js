import {
    TRAININGPLAN_FETCHING_DATA,
    TRAININGPLAN_FETCHING_DATA_SUCCESS,
    TRAININGPLAN_FETCHING_DATA_FAILURE,
} from '../actions/trainingplan'

const defaultState = {
    data: [],
    error: false,
    isFetching: false,
    
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case TRAININGPLAN_FETCHING_DATA:
            return {
                ...state,
                error: false,
                isFetching: true
            };
        case TRAININGPLAN_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: action.data,
                isFetching: false
            };
        case TRAININGPLAN_FETCHING_DATA_FAILURE:
            return {
                ...state,
                error: true,
                isFetching: false
            };
        default:
            return state;
    }
}