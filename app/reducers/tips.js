import {
    TIPS_FETCHING_DATA,
    TIPS_FETCHING_DATA_SUCCESS,
    TIPS_FETCHING_DATA_FAILURE,
} from '../actions/tips'

const defaultState = {
    data: [],
    error: false,
    isFetching: false,
    
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case TIPS_FETCHING_DATA:
            return {
                ...state,
                error: false,
                isFetching: true
            };
        case TIPS_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: action.data,
                isFetching: false
            };
        case TIPS_FETCHING_DATA_FAILURE:
            return {
                ...state,
                error: true,
                isFetching: false
            };
        default:
            return state;
    }
}