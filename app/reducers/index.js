import { combineReducers } from 'redux'
import auth from './auth'
import profile from './profile'
import tips from './tips'
import weight from './weight'
import nutritionalplan from './nutritionalplan'
import trainingplan from './trainingplan'




export default combineReducers( {
    trainingplan,
    nutritionalplan,
    weight,
    tips,
    profile,
    auth,
});