import {
    CHANGE_EMAIL,
    CHANGE_PASSWORD,
    CHANGE_GENRE,
    CHANGE_TALL,
    CHANGE_WEIGHT,
    CHANGE_YEARS,
    CHANGE_GOAL,
    CHANGE_BODYFAT,
    CHANGE_USERNAME,
    CHANGE_PASSWORDCONFIRM,
    USER_FETCHING_DATA,
    USER_FETCHING_DATA_SUCCESS,
    USER_FETCHING_DATA_FAILURE_SIGNIN,
    USER_FETCHING_DATA_FAILURE_SIGNUP,
    INITIAL_STATE
} from '../actions/auth'

const defaultState = {
    username: '',
    email: '',
    password: '',
    genre: '',
    goal: '',
    tall: '',
    bodyFat: '',
    years: '',
    weight: '',
    passwordConfirm: '',
    token: '',
    errorSignUp: false,
    shouldRedirect: false,
    isFetching: false,
    errorSignIn: false
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case INITIAL_STATE:
            return {
                username: '',
                email: '',
                password: '',
                genre: '',
                goal: '',
                tall: '',
                bodyFat: '',
                years: '',
                weight: '',
                passwordConfirm: '',
                token: '',
                errorSignUp: false,
                shouldRedirect: false,
                isFetching: false,
                errorSignIn: false
            };
        case CHANGE_EMAIL:
            return {
                ...state,
                email: action.email
            };
        case CHANGE_PASSWORD:
            return {
                ...state,
                password: action.password
            };
        case CHANGE_PASSWORDCONFIRM:
            return {
                ...state,
                passwordConfirm: action.passwordConfirm
            };
        case CHANGE_USERNAME:
            return {
                ...state,
                username: action.username
            };
        case CHANGE_TALL:
            return {
                ...state,
                tall: action.tall
            };
        case CHANGE_GENRE:
            return {
                ...state,
                genre: action.genre
            };
        case CHANGE_WEIGHT:
            return {
                ...state,
                weight: action.weight
            };
        case CHANGE_GOAL:
            return {
                ...state,
                goal: action.goal
            };
        case CHANGE_BODYFAT:
            return {
                ...state,
                bodyFat: action.bodyFat
            };
        case CHANGE_YEARS:
            return {
                ...state,
                years: action.years
            };
        case USER_FETCHING_DATA:
            return {
                ...state,
                token: '',
                isFetching: true
            };
        case USER_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                token: action.token,
                email: action.email,
                password: action.password,
                isFetching: false,
                shouldRedirect: true
            };
        case USER_FETCHING_DATA_FAILURE_SIGNIN:
            return {
                ...state,
                isFetching: false,
                errorSignIn: true,
                shouldRedirect: false
            };
        case USER_FETCHING_DATA_FAILURE_SIGNUP:
            return {
                ...state,
                isFetching: false,
                errorSignUp: true,
                shouldRedirect: false
            };
        default:
            return state;
    }
}