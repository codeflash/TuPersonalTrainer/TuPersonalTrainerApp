import {
    NUTRITIONALPLAN_FETCHING_DATA,
    NUTRITIONALPLAN_FETCHING_DATA_SUCCESS,
    NUTRITIONALPLAN_FETCHING_DATA_FAILURE,
} from '../actions/nutritionalplan'

const defaultState = {
    data: [],
    error: false,
    isFetching: false,
    
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case NUTRITIONALPLAN_FETCHING_DATA:
            return {
                ...state,
                error: false,
                isFetching: true
            };
        case NUTRITIONALPLAN_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: action.data,
                isFetching: false
            };
        case NUTRITIONALPLAN_FETCHING_DATA_FAILURE:
            return {
                ...state,
                error: true,
                isFetching: false
            };
        default:
            return state;
    }
}