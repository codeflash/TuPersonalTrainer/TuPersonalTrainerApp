import {
    PROFILE_FETCHING_DATA,
    PROFILE_FETCHING_DATA_SUCCESS,
    PROFILE_FETCHING_DATA_FAILURE,
    CHANGE_PASSWORD,
    CHANGE_EMAIL,
    CHANGE_USERNAME,
    INITIAL_STATE
} from '../actions/profile'

const defaultState = {
    data: [],
    error: false,
    isFetching: false,
    username: '',
    email: '',
    password: ''
};

export default(state = defaultState, action) => {
    switch (action.type) {
        case INITIAL_STATE:
            return {
                ...state,
                error: false,
            };
        case CHANGE_EMAIL:
            return {
                ...state,
                email: action.email
            };
        case CHANGE_PASSWORD:
            return {
                ...state,
                password: action.password
            };
        case CHANGE_USERNAME:
            return {
                ...state,
                username: action.username
            };
        case PROFILE_FETCHING_DATA:
            return {
                ...state,
                error: false,
                username: '',
                email: '',
                password: '',
                isFetching: true
            };
        case PROFILE_FETCHING_DATA_SUCCESS:
            return {
                ...state,
                error: false,
                data: action.data,
                username: action.data.nombre,
                email: action.data.correo,
                isFetching: false
            };
        case PROFILE_FETCHING_DATA_FAILURE:
            return {
                ...state,
                error: true,
                isFetching: false
            };
        default:
            return state;
    }
}